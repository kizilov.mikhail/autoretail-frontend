const changeThemes = {
  buttonColor: "#ffffff",
  textColor: "#323332"
}
const topbarTheme = {
  buttonColor: "#ffffff",
  textColor: "#323332"
}
const sidebarTheme = {
  buttonColor: "#323332",
  backgroundColor: undefined,
  textColor: "#788195"
}
const layoutTheme = {
  buttonColor: "#ffffff",
  backgroundColor: "#F1F3F6",
  textColor: undefined
}
const themeConfig = {
  changeThemes,
  topbarTheme,
  sidebarTheme,
  layoutTheme
}

export default themeConfig
