import React from "react"
import { Route, Redirect } from "react-router-dom"
import { ConnectedRouter } from "react-router-redux"

import App from "./app/App"
import SignIn from "./app/containers/Pages/SignIn"

const RestrictedRoute = ({ component: Component, isLoggedIn, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isLoggedIn ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/signin",
            state: { from: props.location }
          }}
        />
      )
    }
  />
)
const PublicRoutes = ({ history, isLoggedIn }) => {
  return (
    <ConnectedRouter history={history}>
      <div>
        <Route exact path={"/signin"} component={SignIn} />
        <RestrictedRoute path="/dashboard" component={App} isLoggedIn={true} />
      </div>
    </ConnectedRouter>
  )
}

export default PublicRoutes
