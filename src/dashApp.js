import React from "react"
import { Provider } from "react-redux"
import { ApolloProvider } from "react-apollo"
import { store, history, client } from "./redux/store"
import PublicRoutes from "./router"

const DashApp = () => (
  <ApolloProvider client={client}>
    <Provider store={store}>
      <PublicRoutes history={history} />
    </Provider>
  </ApolloProvider>
)

export default DashApp
