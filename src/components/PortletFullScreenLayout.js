import React from "react"

class PortletFullScreenLayout extends React.Component {
  render() {
    const { onClickBack, titleText, subTitleText } = this.props
    return (
      <div
        className={
          "m-portlet m-portlet--dark m-portlet--head-solid-bg m-portlet--fullscreen"
        }
      >
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <span class="m-portlet__head-icon" onClick={onClickBack}>
                <i class="la la-arrow-circle-o-left" />
              </span>
              <h3 class="m-portlet__head-text">
                {titleText}
                {subTitleText ? <small>{subTitleText}</small> : null}
              </h3>
            </div>
          </div>
        </div>
        <div class="m-portlet__body">{this.props.children}</div>
      </div>
    )
  }
}

export default PortletFullScreenLayout
