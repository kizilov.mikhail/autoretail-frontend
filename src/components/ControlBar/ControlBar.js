import React from "react"
import { Popconfirm } from "antd"

import Button from "../button"
import ControlBarWrapper from "./controlBar.style"

export default ({ selected, handleAdd, handleDelete }) => {
  return (
    <ControlBarWrapper>
      <div className="leftControls">
        {selected ? (
          <Popconfirm
            title="Вы дейсвтительно хотите удалить?"
            cancelText="Отменить"
            okText="Удалить"
            onConfirm={handleDelete}
          >
            <Button color="danger" iconClass="flaticon-circle">
              Удалить
            </Button>
          </Popconfirm>
        ) : null}
      </div>
      <button
        className="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air"
        onClick={handleAdd}
      >
        <span>
          <i className="flaticon-add" />
          <span>Новый Клиент</span>
        </span>
      </button>
    </ControlBarWrapper>
  )
}
