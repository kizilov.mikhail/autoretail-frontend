import styled from "styled-components"

const ControlBarWrapper = styled.div`
  margin-bottom: 20px;
  display: flex;
  justify-content: space-between;
`

export default ControlBarWrapper
