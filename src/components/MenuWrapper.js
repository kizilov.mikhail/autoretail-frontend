import styled from "styled-components"
import { Menu as AntMenu } from "antd"

const MenuWrapper = styled.div`
  background-color: #fff;

  .ant-menu-vertical .ant-menu-item:after,
  .ant-menu-vertical-left .ant-menu-item:after,
  .ant-menu-vertical-right .ant-menu-item:after,
  .ant-menu-inline .ant-menu-item:after {
    border-right: 3px solid #ff5629;
  }

  .ant-menu {
    background-color: #fff;
    color: #626262;

    :not(.ant-menu-horizontal) {
      .ant-menu-item-selected {
        background-color: #fef6dd;
        color: #626262;

        > a {
          color: #626262;
        }
      }
    }

    .ant-menu-item {
      line-height: 50px;
      height: 50px;
      border-bottom: 1px dashed #ebedf2;
      margin-top: 0;
      margin-bottom: 0;
      font-size: 1.1rem;
      font-weight: 500;

      :hover {
        color: #2c2c2c;
      }

      > a:hover {
        text-decoration: none;
        color: #2c2c2c;
      }

      :not(:last-child) {
        margin-bottom: 0;
      }
    }
  }

  .ant-menu-inline,
  .ant-menu-vertical,
  .ant-menu-vertical-left {
    border-right: none;
  }
`

export default MenuWrapper
