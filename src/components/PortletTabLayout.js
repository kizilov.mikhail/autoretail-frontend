import React from "react"
import { Link } from "react-router-dom"

class PortletTabLayout extends React.Component {
  constructor(props) {
    super(props)
    const { defaultKey, activeTab } = props
    this.state = {
      fullScreen: false,
      activeTab
    }
  }

  toggleFullScreen = () => this.setState({ fullScreen: !this.state.fullScreen })

  render() {
    const { history, titleText, subTitleText, tabs } = this.props
    const { activeTab } = this.state
    const clsFullScreen = this.state.fullScreen ? " m-portlet--fullscreen" : ""
    return (
      <div
        className={
          "m--full-height m-portlet m-portlet--tabs m-portlet--dark m-portlet--head-solid-bg" +
          clsFullScreen
        }
      >
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <span class="m-portlet__head-icon" onClick={history.goBack}>
                <i class="la la-arrow-circle-o-left" />
              </span>
              <h3 class="m-portlet__head-text">
                {titleText}
                {subTitleText ? <small>{subTitleText}</small> : null}
              </h3>
            </div>
          </div>
          <div class="m-portlet__head-tools">
            <ul class="nav nav-tabs m-tabs m-tabs-line">
              {tabs.map(tab => (
                <li
                  key={tab.key}
                  class="nav-item m-tabs__item"
                  onClick={() => this.setState({ activeTab: tab.key })}
                >
                  <Link
                    to={tab.link}
                    className={`nav-link m-tabs__link ${
                      activeTab === tab.key ? "active" : ""
                    }`}
                  >
                    {tab.text}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
          <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item">
                <a
                  onClick={this.toggleFullScreen}
                  class="m-portlet__nav-link m-portlet__nav-link--icon"
                >
                  <i class="la la-expand" />
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="m-portlet__body">{this.props.children}</div>
      </div>
    )
  }
}

export default PortletTabLayout
