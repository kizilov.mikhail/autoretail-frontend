import React from "react"

const Button = props => {
  const { type, iconClass, color, onClick, className } = props

  return (
    <button
      className={`${className ? className : ""} btn ${
        color ? `btn-${color}` : ""
      } m-btn ${iconClass ? "m-btn--icon" : ""}`}
      onClick={onClick}
    >
      <span>
        {iconClass ? <i className={iconClass} /> : null}
        <span>{props.children}</span>
      </span>
    </button>
  )
}

export default Button
