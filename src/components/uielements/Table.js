import React from "react"
import styled from "styled-components"
import { Table } from "antd"

const CustomTable = styled(Table)`
  .ant-checkbox-wrapper:hover &-inner,
  &:hover &-inner,
  &-input:focus + &-inner {
    border-color: #626c75;
  }

  .ant-table-thead {
    > tr {
      > th {
        background-color: #f4f3f8;
        color: #575962;
        font-weight: 600;
      }
    }
  }

  .ant-table {
    &-thead > tr,
    &-tbody > tr {
      &.ant-table-row-hover,
      &:hover {
        & > td {
          background: #fef6dd;
        }
      }
    }

    &-tbody > tr.ant-table-row-selected td {
      background: #fef6dd;
    }
  }

  .ant-checkbox {
    &-inner {
      background: #e5e3ef;
    }
  }

  .ant-checkbox-checked {
    .ant-checkbox-inner {
      background: #626c75;
      border-color: #626c75 !important;
    }
  }
`

export default CustomTable
