import React from "react"

import Button from "../button"

class Portlet extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      fullScreen: false
    }
  }

  toggleFullScreen = () => this.setState({ fullScreen: !this.state.fullScreen })

  render() {
    const { history } = this.props
    const clsFullScreen = this.state.fullScreen ? " m-portlet--fullscreen" : ""
    return (
      <div
        className={
          "m-portlet m-portlet--metal m-portlet--head-solid-bg m-portlet--rounded" +
          clsFullScreen
        }
      >
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <span class="m-portlet__head-icon" onClick={history.goBack}>
                <i class="la la-arrow-circle-o-left" />
              </span>
              <h3 class="m-portlet__head-text">
                Постпление №
                <small>12332131231231231</small>
              </h3>
            </div>
          </div>
          <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item">
                <button type="button" class="btn btn-orange m-btn--boldest">
                  Провести документ
                </button>
              </li>
              <li class="m-portlet__nav-item">
                <button
                  type="button"
                  class="m-btn btn btn-orange m-btn--boldest"
                >
                  Отменить
                </button>
              </li>
            </ul>
          </div>
          <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item">
                <a
                  onClick={this.toggleFullScreen}
                  class="m-portlet__nav-link m-portlet__nav-link--icon"
                >
                  <i class="la la-expand" />
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="m-portlet__body">{this.props.children}</div>
      </div>
    )
  }
}

export default Portlet
