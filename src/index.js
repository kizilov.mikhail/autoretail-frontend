import React from "react"
import ReactDOM from "react-dom"
import { ThemeProvider } from "styled-components"
import { LocaleProvider } from "antd"
import ruRU from "antd/lib/locale-provider/ru_RU"

import theme from "./config/theme"
import DashWrapper from "./DashWrapper.style"
import DashApp from "./dashApp"

ReactDOM.render(
  <LocaleProvider locale={ruRU}>
    <ThemeProvider theme={theme}>
      <DashWrapper>
        <DashApp />
      </DashWrapper>
    </ThemeProvider>
  </LocaleProvider>,
  document.getElementById("root")
)
