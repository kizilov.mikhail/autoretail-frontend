import { createStore, combineReducers, applyMiddleware } from "redux"
import createHistory from "history/createBrowserHistory"
import { routerReducer, routerMiddleware } from "react-router-redux"
import { composeWithDevTools } from "redux-devtools-extension"
import { ApolloClient } from "apollo-client"
import { HttpLink } from "apollo-link-http"
import { InMemoryCache } from "apollo-cache-inmemory"

import reducers from "./reducers"

const client = new ApolloClient({
  link: new HttpLink({
    uri: "http://localhost:8080/graphql",
    opts: {
      credentials: "same-origin"
    }
  }),
  cache: new InMemoryCache({ dataIdFromObject: object => object._id })
})

const history = createHistory()
const routeMiddleware = routerMiddleware(history)
const middlewares = [routeMiddleware]

const store = createStore(
  combineReducers({
    ...reducers,
    router: routerReducer
  }),
  composeWithDevTools(applyMiddleware(...middlewares))
)

export { store, history, client }
