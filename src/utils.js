import moment from "moment"
import localization from "moment/locale/ru"

moment.locale("ru")

const normalizedDate = date => {
  return moment(date).format("ll")
}

export { normalizedDate }
