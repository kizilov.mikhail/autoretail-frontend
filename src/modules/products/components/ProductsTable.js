import React from "react"
import { Link } from "react-router-dom"

import Table from "../../../components/uielements/Table"

const columns = [
  {
    title: "Номер",
    dataIndex: "#"
  },
  {
    title: "Превью",
    dataIndex: "preview"
  },
  {
    title: "Поставка",
    dataIndex: "income.supplier.name"
  },
  {
    title: "Запчасть",
    dataIndex: "category"
  },
  {
    title: "Марка",
    dataIndex: "brand"
  },
  {
    title: "Модель",
    dataIndex: "model"
  },
  {
    title: "Кузов",
    dataIndex: "body"
  },
  {
    title: "Двигатель",
    dataIndex: "engine"
  },
  {
    title: "Год",
    dataIndex: "year"
  },
  {
    title: "П/З",
    dataIndex: "direction_"
  },
  {
    title: "Л/П",
    dataIndex: "side_"
  },
  {
    title: "В/Н",
    dataIndex: "level_"
  },
  {
    title: "Цена",
    dataIndex: "price"
  },
  {
    title: "Закупочная цена",
    dataIndex: "#"
  },
  {
    title: "Склад",
    dataIndex: "inventory.name"
  }
]

class ProductsTable extends React.Component {
  render() {
    const { history, rowSelection, loading, data } = this.props
    return (
      <Table
        rowSelection={rowSelection}
        columns={columns}
        loading={loading}
        dataSource={data}
        rowKey="_id"
        pagination={false}
        scroll={{ x: "140%" }}
        onRow={(record, index) => ({
          onClick: () => {
            history.push(`products/${record._id}`)
          },
          style: { cursor: "pointer" }
        })}
      />
    )
  }
}

export default ProductsTable
