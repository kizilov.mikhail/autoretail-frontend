import React from "react"
import styled from "styled-components"
import { Switch, Route } from "react-router-dom"
import gql from "graphql-tag"
import { graphql, compose } from "react-apollo"
import { notification } from "antd"

import { PortletTabLayout, Button } from "../../../components"
import LayoutContent from "../../../app/components/LayoutContent.style"
import ProductsTable from "../components/ProductsTable"
import { productActions } from "../../../graphql"

const ActionsWrapper = styled.div`
  display: flex;
  align-items: center;
  min-height: 65px;
  ${props =>
    props.selected
      ? "justify-content: space-between;background-color: #fef6dd; padding: 0px 20px; border-bottom: 3px solid #3b4752;"
      : "justify-content: flex-end;"};

  .la {
    color: #f4516c;
    font-size: 2.5rem;
    cursor: pointer;
  }
`

class ProductsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedRowKeys: [],
      visible: false
    }
  }

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys })
  }

  deselectRows = () => this.setState({ selectedRowKeys: [] })

  render() {
    const { allProducts, match: { url } } = this.props
    const { selectedRowKeys } = this.state
    const selected = selectedRowKeys.length > 0
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    }
    return (
      <PortletTabLayout
        titleText="Товары"
        tabs={[
          {
            key: "parts",
            link: `${url}`,
            text: "Автозапчасти"
          },
          {
            key: "wheels",
            link: `${url}/wheels`,
            text: "Шины и диски"
          }
        ]}
        history={this.props.history}
      >
        <ActionsWrapper selected={selected}>
          {selected
            ? [
                <div>
                  <Button color="light" className="m--margin-right-15">
                    Редактировать
                  </Button>
                  <Button color="light" className="m--margin-right-15">
                    Добавить в новую сделку
                  </Button>
                  <Button color="light">Напечатать этикетки</Button>
                </div>,
                <span>
                  <i className="la la-close" onClick={this.deselectRows} />
                </span>
              ]
            : null}
        </ActionsWrapper>
        <Switch>
          <Route
            exact
            path={`${url}`}
            render={() => (
              <ProductsTable
                rowSelection={rowSelection}
                history={this.props.history}
                loading={allProducts.loading}
                data={allProducts.allProducts}
              />
            )}
          />
          <Route
            exact
            path={`${url}/wheels`}
            render={() => (
              <ProductsTable
                rowSelection={rowSelection}
                history={this.props.history}
                loading={allProducts.loading}
                data={allProducts.allProducts}
              />
            )}
          />
        </Switch>
      </PortletTabLayout>
    )
  }
}

export default compose(
  graphql(productActions.allProducts, { name: "allProducts" })
)(ProductsList)
