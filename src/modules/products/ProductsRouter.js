import React from "react"
import { Switch, Route } from "react-router-dom"

import LayoutContentWrapper from "../../app/components/LayoutContentWrapper.style"
import ProductsList from "./containers/ProductsList"

class ProductsRouter extends React.Component {
  render() {
    const { url } = this.props.match
    return (
      <LayoutContentWrapper>
        <Switch>
          <Route path={`${url}`} component={ProductsList} />
        </Switch>
      </LayoutContentWrapper>
    )
  }
}

export default ProductsRouter
