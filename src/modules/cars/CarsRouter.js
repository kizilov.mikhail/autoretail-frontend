import React from "react"
import { Switch, Route } from "react-router-dom"
import LayoutContentWrapper from "../../app/components/LayoutContentWrapper.style"

import CarsList from "./containers/CarsList"
import CarDetails from "./containers/CarDetails"

class CarsRouter extends React.Component {
  render() {
    const { url } = this.props.match
    return (
      <LayoutContentWrapper>
        <Switch>
          <Route exact path={`${url}`} component={CarsList} />
          <Route exact path={`${url}/:id`} component={CarDetails} />
        </Switch>
      </LayoutContentWrapper>
    )
  }
}

export default CarsRouter
