import React from "react"
import { Link } from "react-router-dom"

import Table from "../../../components/uielements/Table"

const columns = [
  {
    title: "Владелец",
    key: "owner",
    render: record => {
      const { owner } = record
      return owner ? (
        <span>
          {owner.surname} {owner.name} {owner.patronymic}
        </span>
      ) : null
    }
  },
  {
    title: "VIN",
    dataIndex: "vin"
  },
  {
    title: "Марка",
    dataIndex: "brand"
  },
  {
    title: "Модель",
    dataIndex: "model"
  },
  {
    title: "Кузов",
    dataIndex: "body"
  },
  {
    title: "Номер кузова",
    dataIndex: "bodyNumber"
  },
  {
    title: "Год",
    dataIndex: "year"
  },
  {
    title: "Гос. номер",
    key: "stateNumber",
    render: record => (
      <span>
        {record.stateNumber}
        <sup>{record.state}</sup>
      </span>
    )
  },
  {
    title: "",
    key: "actions",
    render: record => (
      <Link to={`cars/${record.id}`}>
        <i className="ion-edit" />
      </Link>
    )
  }
]

class CarsTable extends React.Component {
  render() {
    const { rowSelection, data, handleRowClick } = this.props
    return (
      <Table
        rowSelection={rowSelection}
        columns={columns}
        dataSource={data}
        rowKey="_id"
        pagination={false}
        onRow={(record, index) => ({
          onClick: () => handleRowClick(record._id),
          style: { cursor: "pointer" }
        })}
      />
    )
  }
}

export default CarsTable
