import React from "react"
import { Row, Col, Form, Input, Radio, Select, AutoComplete } from "antd"
import { Button } from "antd/lib/radio"

import { PortletFullScreenLayout } from "../../../components"
import ClientsSearch from "../containers/ClientsSearch"

const FormItem = Form.Item
const RadioGroup = Radio.Group
const RadioButton = Radio.Button
const InputGroup = Input.Group
const Option = Select.Option

class CarForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      visible: false,
      selectedClient: null
    }
  }

  handleClientSelect = selectedClient => {
    this.setState({ selectedClient, visible: false })
  }

  toggleClientsSearch = () =>
    this.setState({
      visible: !this.state.visible
    })

  handleSearch = value => {}

  render() {
    const options = {
      rules: [{ required: true, message: "Обязательное поле" }]
    }
    const { getFieldDecorator } = this.props.form
    const { selectedClient, visible } = this.state
    return (
      <div>
        {visible ? (
          <PortletFullScreenLayout
            titleText="Клиенты"
            onClickBack={this.toggleClientsSearch}
          >
            <ClientsSearch handleRowClick={this.handleClientSelect} />
          </PortletFullScreenLayout>
        ) : null}
        <Form className="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
          <div class="form-group m-form__group row">
            <label className="col-lg-2 col-form-label">Владелец:</label>
            <div className="col-lg-8">
              <FormItem>
                {getFieldDecorator("owner", {
                  initialValue: selectedClient ? selectedClient._id : null
                })(
                  <Select disabled={true}>
                    {selectedClient ? (
                      <Option
                        key={selectedClient._id}
                        value={selectedClient._id}
                      >
                        {selectedClient.surname} {selectedClient.name}{" "}
                        {selectedClient.patronymic}
                      </Option>
                    ) : (
                      <Option key="undefined" value={null}>
                        Отсутствует
                      </Option>
                    )}
                  </Select>
                )}
              </FormItem>
            </div>
            <div className="col-lg-2">
              <i
                className="la la-plus"
                onClick={this.toggleClientsSearch}
                style={{
                  cursor: "pointer",
                  fontSize: "2.5rem",
                  fontWeight: "600"
                }}
              />
            </div>
          </div>
          <div className="form-group m-form__group row">
            <div className="col-lg-4">
              <label>Марка:</label>
              <FormItem>
                {getFieldDecorator("brand", options)(
                  <AutoComplete onSearch={this.handleSearch}>
                    <Option key="Toyota">Toyota</Option>
                  </AutoComplete>
                )}
              </FormItem>
            </div>
            <div className="col-lg-4">
              <label>Модель:</label>
              <FormItem>
                {getFieldDecorator("model", options)(
                  <AutoComplete onSearch={this.handleSearch}>
                    <Option key="Corona">Corona</Option>
                  </AutoComplete>
                )}
              </FormItem>
            </div>
            <div className="col-lg-4">
              <label>Кузов:</label>
              <FormItem>
                {getFieldDecorator("body", options)(
                  <AutoComplete onSearch={this.handleSearch}>
                    <Option key="ST190">ST190</Option>
                  </AutoComplete>
                )}
              </FormItem>
            </div>
          </div>
          <div className="form-group m-form__group row">
            <div className="col-lg-6">
              <label>Двигатель:</label>
              <FormItem>
                {getFieldDecorator("engine")(
                  <AutoComplete onSearch={this.handleSearch}>
                    <Option key="G4SF">G4SF</Option>
                  </AutoComplete>
                )}
              </FormItem>
            </div>
            <div className="col-lg-6">
              <label>Год:</label>
              <FormItem>
                {getFieldDecorator("year", options)(
                  <input type="text" class="form-control m-input" />
                )}
              </FormItem>
            </div>
          </div>
          <div className="form-group m-form__group row">
            <div className="col-lg-6">
              <label>Гос. номер:</label>
              <FormItem>
                {getFieldDecorator("stateNumber")(
                  <input type="text" class="form-control m-input" />
                )}
              </FormItem>
            </div>
            <div className="col-lg-6">
              <label>Регион:</label>
              <FormItem>
                {getFieldDecorator("region")(
                  <input type="text" class="form-control m-input" />
                )}
              </FormItem>
            </div>
          </div>
          <div className="form-group m-form__group row">
            <div className="col">
              <label>Комментарий:</label>
              <FormItem>
                {getFieldDecorator("comment")(
                  <textarea class="form-control m-input" rows="2" />
                )}
              </FormItem>
            </div>
          </div>
          <div className="form-group m-form__group row">
            <div className="col">
              <label>Заметки:</label>
              <FormItem>
                {getFieldDecorator("note")(
                  <textarea class="form-control m-input" rows="2" />
                )}
              </FormItem>
            </div>
          </div>
        </Form>
      </div>
    )
  }
}

export default Form.create()(CarForm)
