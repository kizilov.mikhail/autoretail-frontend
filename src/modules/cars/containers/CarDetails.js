import React from "react"
import { Link } from "react-router-dom"
import { Tabs, List, Button } from "antd"

import LayoutContent from "../../../app/components/LayoutContent.style"
import CarForm from "../components/CarForm"
import { PortletLayout } from "../../../components"

const { TabPane } = Tabs
const car = {
  owner: {
    id: "5a4e882d86c44b3daf98e48b",
    name: "Mikhail",
    surname: "Kizilov"
  },
  id: 1,
  brand: "Toyota",
  model: "Corona",
  body: "ST190",
  engine: "G4SF",
  bodyNumber: "23244234234423",
  year: 1999,
  stateNumber: "С077УЕ",
  state: "24"
}

class CarDetails extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      disabled: true
    }
  }

  handleEdit = () => {
    this.setState({
      disabled: false
    })
  }

  handleSave = () => {
    //request for change client info on server
    this.setState({
      disabled: true
    })
  }

  handleCancel = () => {
    this.setState({
      disabled: true
    })
  }

  render() {
    const { match: { url, params: { id } } } = this.props
    return (
      <PortletLayout
        titleText={`Автомобиль № ${id}`}
        history={this.props.history}
      >
        <div class="m-widget13">
          <div className="row">
            <div className="col">
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">
                  Текущий владелец:
                </span>
                <span class="m-widget13__text m-widget13__text-bolder">
                  {car.owner ? (
                    <Link to={`../clients/${car.owner.id}`}>
                      {car.owner.surname} {car.owner.name}{" "}
                      {car.owner.patronymic}
                    </Link>
                  ) : (
                    "Отсутствует"
                  )}
                </span>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4">
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">Марка:</span>
                <span class="m-widget13__text m-widget13__text-bolder">
                  {car.brand}
                </span>
              </div>
            </div>
            <div className="col-lg-4">
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">Модель:</span>
                <span class="m-widget13__text m-widget13__text-bolder">
                  {car.model}
                </span>
              </div>
            </div>
            <div className="col-lg-4">
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">Кузов:</span>
                <span class="m-widget13__text m-widget13__text-bolder">
                  {car.body}
                </span>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4">
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">Двигатель:</span>
                <span class="m-widget13__text m-widget13__text-bolder">
                  {car.engine}
                </span>
              </div>
            </div>
            <div className="col-lg-4">
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">Год:</span>
                <span class="m-widget13__text m-widget13__text-bolder">
                  {car.year}
                </span>
              </div>
            </div>
            <div className="col-lg-4">
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">
                  Номер кузова:
                </span>
                <span class="m-widget13__text m-widget13__text-bolder">
                  {car.bodyNumber}
                </span>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4">
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">Гос номер:</span>
                <span class="m-widget13__text m-widget13__text-bolder">
                  {car.stateNumber} {car.state}
                </span>
              </div>
            </div>
          </div>
          <div class="m-widget13__action m--align-right">
            <button
              type="button"
              class="m-widget__detalis btn m-btn--air btn-info"
            >
              Изменить
            </button>
            <button type="button" class="btn m-btn--air btn-danger">
              Удалить
            </button>
          </div>
        </div>
      </PortletLayout>
    )
  }
}

export default CarDetails
