import React from "react"
import { graphql, compose } from "react-apollo"
import { Modal, notification } from "antd"

import LayoutContent from "../../../app/components/LayoutContent.style"
import CarsTable from "../components/CarsTable"
import CarForm from "../components/CarForm"
import { PortletLayout, Button } from "../../../components"

import ControlBar from "../../../components/ControlBar/ControlBar"
import { carActions } from "../../../graphql"

class CarsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedRowKeys: [],
      visible: false
    }
  }

  handleOk = e => {
    this.form.validateFields((err, values) => {
      if (!err) {
        this.props
          .createCar({
            variables: { input: values }
          })
          .then(res => {
            const { errors, data: { createCar } } = res
            if (errors) {
              notification.error({
                description: errors[0].message,
                message: "Ошибка создания Автомобиля"
              })
            } else {
              notification.success({ message: "Автомобиль создан" })
              this.props.allCars.refetch()
              this.setState({
                visible: false
              })
              this.form.resetFields()
            }
          })
          .catch(err => {
            console.log("Failed")
          })
      }
    })
  }

  handleCancel = e => {
    this.setState({
      visible: false
    })
  }

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys })
  }

  handleAdd = () => this.setState({ visible: true })

  handleDelete = () => {}

  handleRowClick = id => this.props.history.push(`cars/${id}`)

  render() {
    const { visible, selectedRowKeys } = this.state
    const { allCars } = this.props
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    }

    return (
      <PortletLayout titleText="Автомобили" history={this.props.history}>
        <ControlBar
          selected={selectedRowKeys.length > 0}
          handleAdd={this.handleAdd}
          handleDelete={this.handleDelete}
        />
        <CarsTable
          data={allCars.allCars}
          rowSelection={rowSelection}
          handleRowClick={this.handleRowClick}
        />
        <Modal
          footer={[
            <Button color="success" onClick={this.handleOk}>
              Создать
            </Button>,
            <Button color="danger" onClick={this.handleCancel}>
              Отменить
            </Button>
          ]}
          width={700}
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          destroyOnClose={true}
          title="Новый Автомобиль"
          okText="Создать"
          visible={visible}
        >
          <CarForm ref={form => (this.form = form)} />
        </Modal>
      </PortletLayout>
    )
  }
}

CarsList = compose(
  graphql(carActions.allCars, { name: "allCars" }),
  graphql(carActions.createCar, {
    name: "createCar"
  })
)(CarsList)

export default CarsList
