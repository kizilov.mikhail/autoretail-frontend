import React from "react"
import { Form, Input } from "antd"

const FormItem = Form.Item

class CategoryForm extends React.Component {
  componentDidMount() {
    const { client } = this.props
    const { setFieldsValue } = this.props.form
    setFieldsValue(client)
  }

  render() {
    const { getFieldDecorator, resetFields } = this.props.form
    return (
      <Form>
        <FormItem hasFeedback>
          {getFieldDecorator("name", {
            rules: [{ required: true, message: "Обязательное поле" }]
          })(<Input placeholder="Название" />)}
        </FormItem>
      </Form>
    )
  }
}

export default Form.create()(CategoryForm)
