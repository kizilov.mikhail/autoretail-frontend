import React from "react"
import { Switch, Route, Link } from "react-router-dom"
import { Menu } from "antd"

import { MenuWrapper } from "../../components"
import LayoutContentWrapper from "../../app/components/LayoutContentWrapper.style"
import CategoriesList from "./containers/CategoriesList"
import SuppliersList from "./containers/SuppliersList"
import InventoriesList from "./containers/InventoriesList"

class SettingsRouter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      collapsed: false
    }
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

  render() {
    const { url } = this.props.match
    const selectedUrl = this.props.location.pathname.split("/").pop()
    const selectedMenu = selectedUrl === "settings" ? "categories" : selectedUrl
    return (
      <div style={{ display: "flex", height: "100%" }}>
        <MenuWrapper>
          <Menu
            mode="inline"
            defaultSelectedKeys={["categories"]}
            selectedKeys={[selectedMenu]}
            inlineCollapsed={this.state.collapsed}
            style={{ width: "200px" }}
          >
            <Menu.Item key="categories">
              <Link to={`${url}/categories`}>Наименования</Link>
            </Menu.Item>
            <Menu.Item key="suppliers">
              <Link to={`${url}/suppliers`}>Поставщики</Link>
            </Menu.Item>
            <Menu.Item key="inventories">
              <Link to={`${url}/inventories`}>Склады</Link>
            </Menu.Item>
          </Menu>
        </MenuWrapper>
        <LayoutContentWrapper style={{ display: "flex", flexGrow: "1" }}>
          <Switch>
            <Route exact path={`${url}`} component={CategoriesList} />
            <Route
              exact
              path={`${url}/categories`}
              component={CategoriesList}
            />
            <Route exact path={`${url}/suppliers`} component={SuppliersList} />
            <Route
              exact
              path={`${url}/inventories`}
              component={InventoriesList}
            />
          </Switch>
        </LayoutContentWrapper>
      </div>
    )
  }
}

export default SettingsRouter
