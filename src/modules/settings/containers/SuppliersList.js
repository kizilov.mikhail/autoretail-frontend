import React from "react"
import gql from "graphql-tag"
import { graphql, compose } from "react-apollo"
import { Modal, Form, Table, notification } from "antd"

import LayoutContent from "../../../app/components/LayoutContent.style"
import { Button } from "../../../components/uielements"
import CategoryForm from "../components/CategoryForm"
import { supplierActions } from "../../../graphql"

const columns = [
  {
    title: "Навзание",
    dataIndex: "name"
  }
]

class SuppliersList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedRowKeys: [],
      visible: false
    }
  }

  toggleModal = () => {
    this.setState({
      visible: !this.state.visible
    })
  }

  handleOk = () => {
    this.form.validateFields((err, values) => {
      if (!err) {
        this.props
          .createSupplier({
            variables: { input: values },
            update: (store, { data: { createSupplier } }) => {
              const data = store.readQuery({
                query: supplierActions.allSuppliers
              })
              data.allSuppliers.push(createSupplier)
              store.writeQuery({ query: supplierActions.allSuppliers, data })
            }
          })
          .then(res => {
            const { errors } = res
            if (errors) {
              notification.error({
                description: errors[0].message,
                message: "Ошибка создания Поставщика"
              })
            } else {
              notification.success({ message: "Поставщик создан" })
              this.setState({
                visible: false
              })
              this.form.resetFields()
            }
          })
      }
    })
  }

  handleDelete = () => {
    const { selectedRowKeys } = this.state
    this.props
      .deleteCategories({
        variables: { ids: selectedRowKeys },
        refetchQueries: [{ query: supplierActions.allSuppliers }]
      })
      .then(res => {
        notification.success({
          message: "Поставщики удалены"
        })
      })
      .catch(err => {
        console.log("Failed")
      })
    this.setState({
      selectedRowKeys: []
    })
  }

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys })
  }

  render() {
    const { allSuppliers: { allSuppliers = [] } } = this.props
    const { selectedRowKeys } = this.state
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    }
    return (
      <LayoutContent>
        {selectedRowKeys.length > 0 ? (
          <Button type="danger" onClick={this.handleDelete}>
            Удалить
          </Button>
        ) : null}
        <Button type="primary" onClick={this.toggleModal}>
          Новый Поставщик
        </Button>
        <Table
          dataSource={allSuppliers}
          columns={columns}
          pagination={false}
          rowKey="_id"
          rowSelection={rowSelection}
        />
        <Modal
          title="Новый Поставщик"
          okText="Создать"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.toggleModal}
        >
          <CategoryForm ref={form => (this.form = form)} />
        </Modal>
      </LayoutContent>
    )
  }
}

export default compose(
  graphql(supplierActions.allSuppliers, { name: "allSuppliers" }),
  graphql(supplierActions.createSupplier, {
    name: "createSupplier",
    options: {
      errorPolicy: "all"
    }
  }),
  graphql(supplierActions.deleteCategories, { name: "deleteCategories" })
)(SuppliersList)
