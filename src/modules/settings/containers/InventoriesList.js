import React from "react"
import gql from "graphql-tag"
import { graphql, compose } from "react-apollo"
import { Modal, Form, Table, notification } from "antd"

import LayoutContent from "../../../app/components/LayoutContent.style"
import { Button } from "../../../components/uielements"
import CategoryForm from "../components/CategoryForm"
import { inventoryActions } from "../../../graphql"

const columns = [
  {
    title: "Навзание",
    dataIndex: "name"
  }
]

class InventoriesList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedRowKeys: [],
      visible: false
    }
  }

  toggleModal = () => {
    this.setState({
      visible: !this.state.visible
    })
  }

  handleOk = () => {
    this.form.validateFields((err, values) => {
      if (!err) {
        this.props
          .createInventory({
            variables: { input: values },
            update: (store, { data: { createInventory } }) => {
              const data = store.readQuery({
                query: inventoryActions.allInventories
              })
              data.allInventories.push(createInventory)
              store.writeQuery({ query: inventoryActions.allInventories, data })
            }
          })
          .then(res => {
            const { errors } = res
            if (errors) {
              notification.error({
                description: errors[0].message,
                message: "Ошибка создания Склада"
              })
            } else {
              notification.success({ message: "Склад создан" })
              this.setState({
                visible: false
              })
              this.form.resetFields()
            }
          })
      }
    })
  }

  handleDelete = () => {
    const { selectedRowKeys } = this.state
    this.props
      .deleteCategories({
        variables: { ids: selectedRowKeys },
        refetchQueries: [{ query: inventoryActions.allInventories }]
      })
      .then(res => {
        notification.success({
          message: "Склады удалены"
        })
      })
      .catch(err => {
        console.log("Failed")
      })
    this.setState({
      selectedRowKeys: []
    })
  }

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys })
  }

  render() {
    const { allInventories: { allInventories = [] } } = this.props
    const { selectedRowKeys } = this.state
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    }
    return (
      <LayoutContent>
        {selectedRowKeys.length > 0 ? (
          <Button type="danger" onClick={this.handleDelete}>
            Удалить
          </Button>
        ) : null}
        <Button type="primary" onClick={this.toggleModal}>
          Новый Склад
        </Button>
        <Table
          dataSource={allInventories}
          columns={columns}
          pagination={false}
          rowKey="_id"
          rowSelection={rowSelection}
        />
        <Modal
          title="Новый Склад"
          okText="Создать"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.toggleModal}
        >
          <CategoryForm ref={form => (this.form = form)} />
        </Modal>
      </LayoutContent>
    )
  }
}

export default compose(
  graphql(inventoryActions.allInventories, { name: "allInventories" }),
  graphql(inventoryActions.createInventory, {
    name: "createInventory",
    options: {
      errorPolicy: "all"
    }
  }),
  graphql(inventoryActions.deleteInventory, { name: "deleteInventory" })
)(InventoriesList)
