import React from "react"
import gql from "graphql-tag"
import { graphql, compose } from "react-apollo"
import { Modal, Form, Table, notification } from "antd"

import LayoutContent from "../../../app/components/LayoutContent.style"
import { Button } from "../../../components/uielements"
import CategoryForm from "../components/CategoryForm"
import { categoryActions } from "../../../graphql"

const columns = [
  {
    title: "Навзание",
    dataIndex: "name"
  }
]

class CategoriesList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedRowKeys: [],
      visible: false
    }
  }

  toggleModal = () => {
    this.setState({
      visible: !this.state.visible
    })
  }

  handleOk = () => {
    this.form.validateFields((err, values) => {
      if (!err) {
        this.props
          .createCategory({
            variables: { input: values },
            update: (store, { data: { createCategory } }) => {
              const data = store.readQuery({
                query: categoryActions.allCategories
              })
              data.allCategories.push(createCategory)
              store.writeQuery({ query: categoryActions.allCategories, data })
            }
          })
          .then(res => {
            const { errors } = res
            if (errors) {
              notification.error({
                description: errors[0].message,
                message: "Ошибка создания Категории"
              })
            } else {
              notification.success({ message: "Категория создана" })
              this.setState({
                visible: false
              })
              this.form.resetFields()
            }
          })
      }
    })
  }

  handleDelete = () => {
    const { selectedRowKeys } = this.state
    this.props
      .deleteCategories({
        variables: { ids: selectedRowKeys },
        refetchQueries: [{ query: categoryActions.allCategories }]
      })
      .then(res => {
        notification.success({
          message: "Категории(я) удалены"
        })
      })
      .catch(err => {
        console.log("Failed")
      })
    this.setState({
      selectedRowKeys: []
    })
  }

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys })
  }

  render() {
    const { allCategories: { allCategories = [] } } = this.props
    const { selectedRowKeys } = this.state
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    }
    return (
      <LayoutContent>
        {selectedRowKeys.length > 0 ? (
          <Button type="danger" onClick={this.handleDelete}>
            Удалить
          </Button>
        ) : null}
        <Button type="primary" onClick={this.toggleModal}>
          Новая Категория
        </Button>
        <Table
          dataSource={allCategories}
          columns={columns}
          pagination={false}
          rowKey="_id"
          rowSelection={rowSelection}
        />
        <Modal
          title="Новая Категория"
          okText="Создать"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.toggleModal}
        >
          <CategoryForm ref={form => (this.form = form)} />
        </Modal>
      </LayoutContent>
    )
  }
}

export default compose(
  graphql(categoryActions.allCategories, { name: "allCategories" }),
  graphql(categoryActions.createCategory, {
    name: "createCategory",
    options: {
      errorPolicy: "all"
    }
  }),
  graphql(categoryActions.deleteCategories, { name: "deleteCategories" })
)(CategoriesList)
