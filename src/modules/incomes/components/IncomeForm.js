import React from "react"
import { Row, Col, Form, Modal, Input, Radio, Select } from "antd"

const FormItem = Form.Item
const RadioGroup = Radio.Group
const RadioButton = Radio.Button
const { TextArea } = Input

class IncomeForm extends React.Component {
  render() {
    const { suppliers, inventories } = this.props
    const { getFieldDecorator } = this.props.form
    return (
      <Form>
        <FormItem label="Поступление" hasFeedback>
          {getFieldDecorator("supplier", {
            rules: [{ required: true, message: "Обязательное поле" }]
          })(
            <Select>
              {suppliers.map(s => (
                <Select.Option key={s._id} value={s._id}>
                  {s.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem label="Склад" hasFeedback>
          {getFieldDecorator("inventory", {
            rules: [{ required: true, message: "Обязательное поле" }]
          })(
            <Select>
              {inventories.map(i => (
                <Select.Option key={i._id} value={i._id}>
                  {i.name}
                </Select.Option>
              ))}
            </Select>
          )}
        </FormItem>
        <FormItem label="Комментарий">
          {getFieldDecorator("comment")(<TextArea rows={2} />)}
        </FormItem>
      </Form>
    )
  }
}

export default Form.create()(IncomeForm)
