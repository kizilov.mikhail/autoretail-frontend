import React from "react"
import styled from "styled-components"
import { palette } from "styled-theme"
import {
  Row,
  Col,
  Form,
  DatePicker,
  Input,
  InputNumber,
  Radio,
  AutoComplete,
  Switch,
  Select,
  Upload
} from "antd"

import { Button } from "../../../components/uielements"

const FormItem = Form.Item
const RadioGroup = Radio.Group
const RadioButton = Radio.Button
const InputGroup = Input.Group
const Option = Select.Option

const ProductFormWrapper = styled.div`
  background-color: ${palette("secondary", 4)};
  padding: 90px 15px 20px 15px;
  height: 100vh;
  width: 380px;
  position: fixed;
  overflow-y: scroll;
  top: 0;
  right: 0;

  .ant-btn {
    width: 100%;
  }

  .ant-form-item-label {
    label {
      color: ${palette("grayscale", 6)};
    }
  }
`

class ProductForm extends React.Component {
  render() {
    const {
      handleOk,
      handleCancel,
      handleChange,
      handleSearch,
      brands,
      models,
      bodies,
      engines
    } = this.props
    const { getFieldDecorator } = this.props.form
    const rowLayout = {
      gutter: 16,
      type: "flex",
      justify: "space-between"
    }
    return (
      <ProductFormWrapper>
        <Form layout="vertical">
          <FormItem label="Розничная цена" hasFeedback>
            {getFieldDecorator("price", {
              rules: [{ required: true, message: "Обязательное поле" }]
            })(<InputNumber min={0} max={1000000} />)}
          </FormItem>
          <Row {...rowLayout}>
            <Col span={12}>
              <FormItem label="Марка" hasFeedback>
                {getFieldDecorator("brand", {
                  rules: [{ required: true, message: "Обязательное поле" }]
                })(
                  <Select
                    showSearch
                    placeholder="Марка"
                    optionFilterProp="children"
                    onChange={handleChange}
                    onSearch={handleSearch}
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {brands.map(brand => (
                      <Select.Option key={brand._id} value={brand.name}>
                        {brand.name}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="Модель" hasFeedback>
                {getFieldDecorator("model", {
                  rules: [{ required: true, message: "Обязательное поле" }]
                })(
                  <Select
                    showSearch
                    placeholder="Модель"
                    optionFilterProp="children"
                    onChange={handleChange}
                    onSearch={handleSearch}
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {models.map(model => (
                      <Select.Option key={model._id} value={model.name}>
                        {model.name}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row {...rowLayout}>
            <Col span={8}>
              <FormItem label="Кузов" hasFeedback>
                {getFieldDecorator("body", {
                  rules: [{ required: true, message: "Обязательное поле" }]
                })(
                  <Select
                    showSearch
                    placeholder="Кузов"
                    optionFilterProp="children"
                    onChange={handleChange}
                    onSearch={handleSearch}
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {bodies.map(body => (
                      <Select.Option key={body._id} value={body.name}>
                        {body.name}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="Двигатель">
                {getFieldDecorator("engine")(
                  <Select
                    showSearch
                    placeholder="Двигатель"
                    optionFilterProp="children"
                    onChange={handleChange}
                    onSearch={handleSearch}
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {engines.map(engine => (
                      <Select.Option key={engine._id} value={engine.name}>
                        {engine.name}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="Год">
                {getFieldDecorator("year")(<Input />)}
              </FormItem>
            </Col>
          </Row>
          <FormItem label="Конфигурация автомобиля">
            {getFieldDecorator("configuration")(<Input />)}
          </FormItem>
          <Row {...rowLayout}>
            <Col span={8}>
              <FormItem label="Маркер">
                {getFieldDecorator("mark")(<Input />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="Производитель">
                {getFieldDecorator("manufacturer")(<Input />)}
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="Цвет">
                {getFieldDecorator("color")(<Input />)}
              </FormItem>
            </Col>
          </Row>
          <Row {...rowLayout}>
            <Col span={12}>
              <FormItem label="ОЕМ номер">
                {getFieldDecorator("oemNumber")(<Input />)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="Номер производителя">
                {getFieldDecorator("manufacturerNumber")(<Input />)}
              </FormItem>
            </Col>
          </Row>
          <FormItem label="Категория" hasFeedback>
            {getFieldDecorator("category", {
              rules: [{ required: true, message: "Обязательное поле" }]
            })(<Input />)}
          </FormItem>
          <FormItem label="Комментарий">
            {getFieldDecorator("comment")(<Input />)}
          </FormItem>
          <FormItem label="Заметка">
            {getFieldDecorator("note")(<Input />)}
          </FormItem>
          <Row {...rowLayout}>
            <Col span={12}>
              <Button type="default" onClick={handleCancel}>
                Отменить
              </Button>
            </Col>
            <Col span={12}>
              <Button type="primary" onClick={handleOk}>
                Создать
              </Button>
            </Col>
          </Row>
        </Form>
      </ProductFormWrapper>
    )
  }
}

export default Form.create()(ProductForm)
