import React from "react"
import { Link } from "react-router-dom"

import Table from "../../../components/uielements/Table"
import { normalizedDate } from "../../../utils"

const columns = [
  {
    title: "Номер/дата",
    dataIndex: "createdAt",
    render: date => normalizedDate(date)
  },
  { title: "Поставка", dataIndex: "supplier.name" },
  { title: "Склад", dataIndex: "inventory.name" },
  { title: "Сумма (руб.)", dataIndex: "total" },
  { title: "Статус", dataIndex: "status_" },
  { title: "Комментарий", dataIndex: "comment" }
]

class IncomesTable extends React.Component {
  render() {
    const { history, rowSelection, data, loading } = this.props
    return (
      <Table
        onRow={(record, index) => ({
          onClick: () => {
            history.push(`incomes/${record._id}`)
          },
          style: { cursor: "pointer" }
        })}
        rowSelection={rowSelection}
        columns={columns}
        dataSource={data}
        loading={loading}
        rowKey="_id"
        pagination={false}
      />
    )
  }
}

export default IncomesTable
