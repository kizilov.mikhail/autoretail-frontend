import React from "react"

import Table from "../../../components/uielements/Table"

const columns = [
  {
    title: "Код",
    dataIndex: "_id"
  },
  {
    title: "Название",
    key: "name",
    render: record => (
      <span>
        {record.category} {record.direction_} {record.side_} {record.level_}{" "}
        {record.brand} {record.model} {record.body}
      </span>
    )
  },
  {
    title: "Количество",
    dataIndex: "quantity"
  },
  {
    title: "Цена(руб.)",
    dataIndex: "price"
  }
]

class ProductsTable extends React.Component {
  render() {
    const { data, loading, rowSelection } = this.props
    return (
      <Table
        loading={loading}
        rowSelection={rowSelection}
        columns={columns}
        dataSource={data}
        rowKey="_id"
        pagination={false}
      />
    )
  }
}

export default ProductsTable
