import React from "react"
import { Switch, Route } from "react-router-dom"

import LayoutContentWrapper from "../../app/components/LayoutContentWrapper.style"
import IncomesList from "./containers/IncomesList"
import NewIncome from "./containers/NewIncome"

class IncomesRouter extends React.Component {
  render() {
    const { url } = this.props.match
    return (
      <Switch>
        <Route exact path={`${url}`} component={IncomesList} />
        <Route exact path={`${url}/:id`} component={NewIncome} />
      </Switch>
    )
  }
}

export default IncomesRouter
