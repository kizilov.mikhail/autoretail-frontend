import React from "react"
import styled from "styled-components"
import debounce from "debounce"
import { graphql, compose } from "react-apollo"
import { Row, Col, Divider, notification, Spin } from "antd"

import { PortletActionsLayout, Button } from "../../../components"
import LayoutContent from "../../../app/components/LayoutContent.style"
import ProductsTable from "../components/ProductsTable"
import IncomeForm from "../components/IncomeForm"
import ProductForm from "../components/ProductForm"

import {
  incomeActions,
  productActions,
  carCatalogActions
} from "../../../graphql"

import LayoutContentWrapper from "../../../app/components/LayoutContentWrapper.style"

const ControBarWrapper = styled.div`
  display: flex;
  justify-content: ${props => (props.selected ? "space-between" : "flex-end")};
  margin-bottom: 20px;
`

const ActionsWrapper = styled.div`
  display: flex;
  align-items: center;
  min-height: 65px;
  ${props =>
    props.selected
      ? "justify-content: space-between;background-color: #fef6dd; padding: 0px 20px; border-bottom: 3px solid #3b4752;"
      : "justify-content: flex-end;"};

  .la {
    color: #f4516c;
    font-size: 2.5rem;
    cursor: pointer;
  }
`

class NewIncome extends React.Component {
  constructor(props) {
    super(props)
    this.handleModelChange = debounce(this.handleModelChange.bind(this), 700)
    this.state = {
      selectedRowKeys: [],
      visible: false,
      brands: [],
      models: [],
      bodies: [],
      engines: []
    }
  }

  handleAdd = () => {
    this.setState({
      visible: true
    })
  }

  handleOk = e => {
    this.form.validateFields((err, values) => {
      const { match: { params: { id } } } = this.props
      if (!err) {
        this.props
          .createProduct({
            variables: { input: { ...values, income: id } }
          })
          .then(res => {
            const { errors } = res
            if (errors) {
              notification.error({
                description: errors[0].message,
                message: "Ошибка создания Товара"
              })
            } else {
              notification.success({ message: "Товар создан" })
              this.setState({
                visible: false
              })
              this.form.resetFields()
            }
          })
          .catch(err => {
            console.log("Failed")
          })
      }
    })
  }

  handleCancel = () => {
    this.setState({
      visible: false
    })
  }

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys })
  }

  renderIncomeActions = status => {
    if (status === "draft")
      return [
        <Button
          color="orange"
          className="m--margin-right-20 m-btn--boldest"
          onClick={() => this.handleUpdateIncomeStatus("completed")}
        >
          Провести документ
        </Button>,
        <Button
          color="orange"
          className="m-btn--boldest"
          onClick={() => this.handleUpdateIncomeStatus("cancelled")}
        >
          Отменить
        </Button>
      ]
    if (status === "cancelled")
      return [
        <Button
          color="orange"
          onClick={() => this.handleUpdateIncomeStatus("draft")}
        >
          Вернуть в работу
        </Button>
      ]
    return []
  }

  deselectRows = () => this.setState({ selectedRowKeys: [] })

  renderControls = () => {
    const { selectedRowKeys } = this.state
    const selected = selectedRowKeys.length > 0
    if (selected) {
      return (
        <ControBarWrapper selected={selected}>
          <Button type="danger">Удалить</Button>
          {selectedRowKeys.length < 2 ? (
            <Button type="primary">Создать на основе</Button>
          ) : null}
        </ControBarWrapper>
      )
    } else {
      return (
        <ControBarWrapper selected={selected}>
          <Button
            iconClass="flaticon-add"
            color="info"
            onClick={this.handleAdd}
          >
            Добавить Товар
          </Button>
        </ControBarWrapper>
      )
    }
  }

  handleUpdateIncomeStatus = status => {
    const { match: { params: { id } } } = this.props
    this.props
      .updateIncomeStatus({
        variables: { input: { id, status } }
      })
      .then(res => {
        const { errors } = res
        if (errors) {
          notification.error({
            description: errors[0].message,
            message: "Ошибка обновления Поступления"
          })
        } else {
          notification.success({ message: "Поступление обновлено" })
        }
      })
      .catch(err => {
        console.log("Failed")
      })
  }

  onSelectLolChange = value => {
    console.log(value)
  }

  handleModelChange(value) {
    this.props.carSuggestions.refetch({ model: value }).then(res => {
      const { data: { carSuggestions } } = res
      if (carSuggestions) {
        if (carSuggestions.length < 1) {
          return
        }
        const brands = carSuggestions.filter(o => o.category === "brand")
        const models = carSuggestions.filter(o => o.category === "model")
        const bodies = carSuggestions.filter(o => o.category === "body")
        const engines = carSuggestions.filter(o => o.category === "engine")
        this.setState({
          brands,
          models,
          bodies,
          engines
        })
      }
    })
  }

  render() {
    const { visible, selectedRowKeys } = this.state
    const { income, match: { params: { id } } } = this.props
    const products = income.loading ? [] : income.income.products
    const marginRight = visible ? "380px" : "0px"
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    }
    const selected = selectedRowKeys.length > 0
    if (income.loading)
      return (
        <LayoutContentWrapper>
          <LayoutContent
            style={{
              textAlign: "center"
            }}
          >
            <Spin />
          </LayoutContent>
        </LayoutContentWrapper>
      )
    else {
      return (
        <LayoutContentWrapper>
          <PortletActionsLayout
            titleText={`Поступление № ${id}`}
            history={this.props.history}
            actions={this.renderIncomeActions(income.income.status)}
          >
            <div className="row m-row--col-separator-lg">
              <div className="col-lg-4">
                <div className="m-widget13">
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Поступление:
                    </span>
                    <span class="m-widget13__text m-widget13__text-bolder">
                      {income.income.supplier.name}
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">Склад:</span>
                    <span class="m-widget13__text m-widget13__text-bolder">
                      {income.income.inventory.name}
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Комментарий:
                    </span>
                    <span class="m-widget13__text m-widget13__text-bolder">
                      {income.income.comment}
                    </span>
                  </div>
                </div>
              </div>
              <div className="col-lg-8">
                <ActionsWrapper selected={selected}>
                  {selected ? (
                    [
                      <div>
                        <Button color="light" className="m--margin-right-15">
                          Удалить
                        </Button>
                        <Button color="light" className="m--margin-right-15">
                          Создать на основе
                        </Button>
                      </div>,
                      <span>
                        <i
                          className="la la-close"
                          onClick={this.deselectRows}
                        />
                      </span>
                    ]
                  ) : (
                    <div>
                      <Button
                        iconClass="flaticon-add"
                        color="info"
                        onClick={this.handleAdd}
                      >
                        Добавить Товар
                      </Button>
                    </div>
                  )}
                </ActionsWrapper>
                {income.income.status === "draft" ? (
                  <ProductsTable
                    loading={income.loading}
                    data={products}
                    rowSelection={rowSelection}
                  />
                ) : (
                  <ProductsTable loading={income.loading} data={products} />
                )}
              </div>
            </div>
            {visible ? (
              <ProductForm
                handleChange={this.onSelectChange}
                handleSearch={this.handleModelChange}
                handleCancel={this.handleCancel}
                handleOk={this.handleOk}
                ref={form => (this.form = form)}
                brands={this.state.brands}
                models={this.state.models}
                bodies={this.state.bodies}
                engines={this.state.engines}
              />
            ) : null}
          </PortletActionsLayout>
        </LayoutContentWrapper>
      )
    }
  }
}

export default compose(
  graphql(incomeActions.income, {
    name: "income",
    options: ({ match: { params: { id } } }) => ({ variables: { id } })
  }),
  graphql(productActions.createProduct, {
    name: "createProduct",
    options: { refetchQueries: ["income"] }
  }),
  graphql(incomeActions.updateIncomeStatus, {
    name: "updateIncomeStatus",
    options: { refetchQueries: ["income"] }
  }),
  graphql(carCatalogActions.carSuggestions, {
    name: "carSuggestions"
  })
)(NewIncome)
