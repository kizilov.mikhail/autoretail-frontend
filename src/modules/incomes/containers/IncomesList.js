import React from "react"
import { graphql, compose } from "react-apollo"
import { Modal, Tabs, Menu, notification } from "antd"

import {
  incomeActions,
  supplierActions,
  inventoryActions
} from "../../../graphql"

import LayoutContentWrapper from "../../../app/components/LayoutContentWrapper.style"
import LayoutContent from "../../../app/components/LayoutContent.style"
import { PortletLayout, Button, MenuWrapper } from "../../../components"
import IncomesTable from "../components/IncomesTable"
import IncomeForm from "../components/IncomeForm"

class IncomesList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedRowKeys: [],
      visible: false
    }
  }

  handleOk = e => {
    this.form.validateFields((err, values) => {
      if (!err) {
        this.props
          .createIncome({
            variables: { input: values }
          })
          .then(res => {
            const { errors, data: { createIncome } } = res
            if (errors) {
              notification.error({
                description: errors[0].message,
                message: "Ошибка создания Поступления"
              })
            } else {
              notification.success({ message: "Поступление создано" })
              this.setState({
                visible: false
              })
              this.form.resetFields()
              this.props.history.push(`incomes/${createIncome._id}`)
            }
          })
          .catch(err => {
            console.log("Failed")
          })
      }
    })
  }
  handleCancel = e => {
    this.setState({
      visible: false
    })
  }

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys })
  }

  onMenuItemClick = ({ key }) => {
    const { allIncomes: { refetch } } = this.props
    const filter = key === "all" ? {} : { status: key }
    refetch({ filter })
  }

  handleAdd = () => this.setState({ visible: true })

  handleDelete = () => {}

  render() {
    const { visible, selectedRowKeys } = this.state
    const {
      allIncomes,
      allSuppliers,
      allInventories,
      match: { url }
    } = this.props
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    }

    return (
      <div style={{ display: "flex", height: "100%" }}>
        <MenuWrapper>
          <Menu
            mode="inline"
            onClick={this.onMenuItemClick}
            defaultSelectedKeys={["draft"]}
            inlineCollapsed={this.state.collapsed}
            style={{ width: "200px" }}
          >
            <Menu.Item key="draft">Черновики</Menu.Item>
            <Menu.Item key="completed">Выполненные</Menu.Item>
            <Menu.Item key="cancelled">Отмененные</Menu.Item>
            <Menu.Item key="all">Все поступления</Menu.Item>
          </Menu>
        </MenuWrapper>
        <LayoutContentWrapper style={{ flexGrow: "1" }}>
          <PortletLayout titleText="Поступления" history={this.props.history}>
            <div className="m--margin-bottom-20 m--align-right">
              <Button
                iconClass="flaticon-add"
                onClick={this.handleAdd}
                color="info"
              >
                Новое Поступление
              </Button>
            </div>
            <IncomesTable
              history={this.props.history}
              data={allIncomes.allIncomes}
              loading={allIncomes.loading}
            />
            <Modal
              title="Новое Поступление"
              visible={visible}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
              okText="Создать"
            >
              <IncomeForm
                suppliers={allSuppliers.allSuppliers}
                inventories={allInventories.allInventories}
                ref={form => (this.form = form)}
              />
            </Modal>
          </PortletLayout>
        </LayoutContentWrapper>
      </div>
    )
  }
}

export default compose(
  graphql(incomeActions.allIncomes, {
    name: "allIncomes",
    options: { variables: { filter: { status: "draft" } } }
  }),
  graphql(incomeActions.createIncome, { name: "createIncome" }),
  graphql(inventoryActions.allInventories, { name: "allInventories" }),
  graphql(supplierActions.allSuppliers, { name: "allSuppliers" })
)(IncomesList)
