import React from "react"
import { Switch, Route } from "react-router-dom"
import LayoutContentWrapper from "../../app/components/LayoutContentWrapper.style"

import ClientsList from "./containers/ClientsList"
import ClientDetails from "./containers/ClientDetails"

class ClientsRouter extends React.Component {
  render() {
    const { url } = this.props.match
    return (
      <LayoutContentWrapper>
        <Switch>
          <Route exact path={`${url}`} component={ClientsList} />
          <Route path={`${url}/:id`} component={ClientDetails} />
        </Switch>
      </LayoutContentWrapper>
    )
  }
}

export default ClientsRouter
