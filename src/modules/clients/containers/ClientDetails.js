import React from "react"
import { Link, Switch, Route } from "react-router-dom"
import { Tabs, List, Button } from "antd"

import LayoutContent from "../../../app/components/LayoutContent.style"
import ClientForm from "../components/ClientForm"
import { PortletTabLayout } from "../../../components"

const { TabPane } = Tabs
const client = {
  _id: "5a51d26f13543249d487672a",
  name: "Mikhail",
  surname: "Kizilov",
  birthDay: "01.02.1996",
  sex: "M",
  phone: "+79832685910",
  email: "misha.k_@mail.ru"
}
const cars = [
  {
    id: 1,
    brand: "Nissan",
    model: "Cube"
  }
]

class ClientDetails extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      disabled: true
    }
  }

  handleEdit = () => {
    this.setState({
      disabled: false
    })
  }

  handleSave = () => {
    //request for change client info on server
    this.setState({
      disabled: true
    })
  }

  handleCancel = () => {
    this.form.validateFields((err, values) => {
      if (!err) {
        console.log(values)
      }
    })
    this.setState({
      disabled: true
    })
  }

  render() {
    const { match: { url, params: { id, tab } } } = this.props
    const { disabled } = this.state
    console.log(this.props)
    return (
      <PortletTabLayout
        titleText={`Клиент № ${id}`}
        history={this.props.history}
        defaultKey="client"
        activeTab={tab}
        tabs={[
          {
            key: "client",
            link: `${url}`,
            text: "Клиент"
          },
          {
            key: "cars",
            link: `${url}/cars`,
            text: "Автомобили"
          },
          {
            key: "sales",
            link: `${url}/sales`,
            text: "Сделки"
          },
          {
            key: "services",
            link: `${url}/services`,
            text: "Обслуживание"
          }
        ]}
      >
        <Switch>
          <Route
            path={`${url}/cars`}
            render={() => (
              <div class="m-widget11">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <td class="m-widget11">Марка</td>
                        <td class="m-widget11">Модель</td>
                        <td class="m-widget11">Кузов</td>
                        <td class="m-widget11">Двигатель</td>
                        <td class="m-widget11">Гос номер</td>
                        <td class="m-widget11">Пробег</td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <span class="m-widget11__title">Vertex 2.0</span>
                          <span class="m-widget11__sub">
                            Vertex To By Again
                          </span>
                        </td>
                        <td>19,200</td>
                        <td>$63</td>
                        <td class="m--align-right m--font-brand">$14,740</td>
                      </tr>
                      <tr>
                        <td>
                          <span class="m-widget11__title">Metronic</span>
                          <span class="m-widget11__sub">
                            Powerful Admin Theme
                          </span>
                        </td>
                        <td>24,310</td>
                        <td>$39</td>
                        <td class="m--align-right m--font-brand">$16,010</td>
                      </tr>
                      <tr>
                        <td>
                          <span class="m-widget11__title">Apex</span>
                          <span class="m-widget11__sub">
                            The Best Selling App
                          </span>
                        </td>
                        <td>9,076</td>
                        <td>$105</td>
                        <td class="m--align-right m--font-brand">$37,200</td>
                      </tr>
                      <tr>
                        <td>
                          <span class="m-widget11__title">Cascades</span>
                          <span class="m-widget11__sub">Design Tool</span>
                        </td>
                        <td>11,094</td>
                        <td>$16</td>
                        <td class="m--align-right m--font-brand">$8,520</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="m-widget11__action m--align-right">
                  <button
                    type="button"
                    class="btn m-btn--pill btn-outline-brand m-btn m-btn--custom"
                  >
                    Import Report
                  </button>
                </div>
              </div>
              // <List
              //   itemLayout="horizontal"
              //   dataSource={cars}
              //   renderItem={item => (
              //     <List.Item
              //       actions={[
              //         <Link to={`../cars/${item.id}/edit`}>
              //           <i className="ion-edit" />
              //         </Link>,
              //         <Link to={`../cars/${item.id}/delete`}>
              //           <i className="ion-trash-a" />
              //         </Link>
              //       ]}
              //     >
              //       <List.Item.Meta
              //         title={
              //           <a href="https://ant.design">
              //             {item.brand} {item.model}
              //           </a>
              //         }
              //         description="Ant Design, a design language for background applications, is refined by Ant UED Team"
              //       />
              //       <div>content</div>
              //     </List.Item>
              //   )}
              // />
            )}
          />
          <Route
            path={`${url}/sales`}
            render={() => (
              <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                <div class="m-portlet__body">
                  <div class="form-group m-form__group row">
                    <div class="col-lg-4">
                      <label>Full Name:</label>
                      <input
                        type="email"
                        class="form-control m-input"
                        placeholder="Enter full name"
                      />
                      <span class="m-form__help">
                        Please enter your full name
                      </span>
                    </div>
                    <div class="col-lg-4">
                      <label class="">Email:</label>
                      <input
                        type="email"
                        class="form-control m-input"
                        placeholder="Enter email"
                      />
                      <span class="m-form__help">Please enter your email</span>
                    </div>
                    <div class="col-lg-4">
                      <label>Username:</label>
                      <div class="input-group m-input-group m-input-group--square">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="la la-user" />
                          </span>
                        </div>
                        <input
                          type="text"
                          class="form-control m-input"
                          placeholder=""
                        />
                      </div>
                      <span class="m-form__help">
                        Please enter your username
                      </span>
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <div class="col-lg-4">
                      <label class="">Contact:</label>
                      <input
                        type="email"
                        class="form-control m-input"
                        placeholder="Enter contact number"
                      />
                      <span class="m-form__help">
                        Please enter your contact
                      </span>
                    </div>
                    <div class="col-lg-4">
                      <label class="">Fax:</label>
                      <div class="m-input-icon m-input-icon--right">
                        <input
                          type="text"
                          class="form-control m-input"
                          placeholder="Fax number"
                        />
                        <span class="m-input-icon__icon m-input-icon__icon--right">
                          <span>
                            <i class="la la-info-circle" />
                          </span>
                        </span>
                      </div>
                      <span class="m-form__help">Please enter fax</span>
                    </div>
                    <div class="col-lg-4">
                      <label>Address:</label>
                      <div class="m-input-icon m-input-icon--right">
                        <input
                          type="text"
                          class="form-control m-input"
                          placeholder="Enter your address"
                        />
                        <span class="m-input-icon__icon m-input-icon__icon--right">
                          <span>
                            <i class="la la-map-marker" />
                          </span>
                        </span>
                      </div>
                      <span class="m-form__help">
                        Please enter your address
                      </span>
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <div class="col-lg-4">
                      <label class="">Postcode:</label>
                      <div class="m-input-icon m-input-icon--right">
                        <input
                          type="text"
                          class="form-control m-input"
                          placeholder="Enter your postcode"
                        />
                        <span class="m-input-icon__icon m-input-icon__icon--right">
                          <span>
                            <i class="la la-bookmark-o" />
                          </span>
                        </span>
                      </div>
                      <span class="m-form__help">
                        Please enter your postcode
                      </span>
                    </div>
                    <div class="col-lg-4">
                      <label class="">User Group:</label>
                      <div class="m-radio-inline">
                        <label class="m-radio m-radio--solid">
                          <input type="radio" name="example_2" value="2" />{" "}
                          Sales Person
                          <span />
                        </label>
                        <label class="m-radio m-radio--solid">
                          <input type="radio" name="example_2" value="2" />{" "}
                          Customer
                          <span />
                        </label>
                      </div>
                      <span class="m-form__help">Please select user group</span>
                    </div>
                  </div>
                </div>
              </form>
            )}
          />
          <Route
            path={`${url}/services`}
            render={() => <span>Services</span>}
          />
          <Route
            path={`${url}`}
            render={
              () => (
                <div class="m-widget13">
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">ФИО:</span>
                    <span class="m-widget13__text m-widget13__text-bolder">
                      {client.name} {client.surname}
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Anual Taxes:
                    </span>
                    <span class="m-widget13__text m-widget13__text-bolder">
                      $520,000
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Business Description:
                    </span>
                    <span class="m-widget13__text">
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry.
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Employee Amount:
                    </span>
                    <span class="m-widget13__text m-widget13__text-bolder">
                      1,300
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">Emal:</span>
                    <span class="m-widget13__text">
                      supporet@keenthemes.com
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Телефон:
                    </span>
                    <span class="m-widget13__text m-widget13__number-bolder m--font-brand">
                      {client.phone}
                    </span>
                  </div>
                  <div class="m-widget13__action m--align-right">
                    <button
                      type="button"
                      class="m-widget__detalis btn m-btn--air btn-info"
                    >
                      Изменить
                    </button>
                    <button type="button" class="btn m-btn--air btn-danger">
                      Удалить
                    </button>
                  </div>
                </div>
              )
              //   () => (
              //   <ClientForm
              //     ref={form => (this.form = form)}
              //     client={client}
              //     disabled={disabled}
              //     handleEdit={this.handleEdit}
              //     handleSave={this.handleSave}
              //     handleCancel={this.handleCancel}
              //   />
              // )
            }
          />
        </Switch>
      </PortletTabLayout>
    )
  }
}

export default ClientDetails
