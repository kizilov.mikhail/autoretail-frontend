import React from "react"
import gql from "graphql-tag"
import { graphql, compose } from "react-apollo"
import { notification } from "antd"

import LayoutContent from "../../../app/components/LayoutContent.style"
import ClientsTable from "../components/ClientsTable"
import ControlBar from "../../../components/ControlBar/ControlBar"
import { PortletLayout } from "../../../components"

import ControlBarWrapper from "../components/controlBar.style"
import AddForm from "../components/AddForm"
import { clientActions } from "../../../graphql"

class ClientsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedRowKeys: [],
      visible: false
    }
  }

  showModal = () => this.setState({ visible: true })

  handleOk = e => {
    this.form.validateFields((err, values) => {
      if (!err) {
        this.props
          .createClient({
            variables: { input: values },
            update: (store, { data: { createClient } }) => {
              const data = store.readQuery({
                query: clientActions.allClients
              })
              data.allClients.push(createClient)
              store.writeQuery({ query: clientActions.allClients, data })
            }
          })
          .then(res => {
            const { errors } = res
            if (errors) {
              notification.error({
                description: errors[0].message,
                message: "Ошибка создания Клиента"
              })
            } else {
              notification.success({ message: "Пользователь создан" })
              this.setState({
                visible: false
              })
              this.form.resetFields()
            }
          })
          .catch(err => {
            console.log("Failed")
          })
      }
    })
  }
  handleCancel = e => {
    this.setState({
      visible: false
    })
  }

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys })
  }

  handleDelete = () => {
    const { selectedRowKeys } = this.state
    this.props
      .deleteClient({ variables: { ids: selectedRowKeys } })
      .then(res => {
        notification.success({
          message: "Клиент(ы) удалены"
        })
      })
      .catch(err => {
        console.log("Failed")
      })
    this.setState({
      selectedRowKeys: []
    })
  }

  handleRowClick = id => this.props.history.push(`clients/${id}`)

  render() {
    const { allClients } = this.props
    const { selectedRowKeys } = this.state
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange
    }
    return (
      <PortletLayout titleText="Клиенты" history={this.props.history}>
        <ControlBar
          handleAdd={this.showModal}
          handleDelete={this.handleDelete}
          selected={selectedRowKeys.length > 0}
        />
        <ClientsTable
          handleRowClick={this.handleRowClick}
          rowSelection={rowSelection}
          loading={allClients.loading}
          data={allClients.allClients}
        />
        <AddForm
          ref={form => (this.form = form)}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        />
      </PortletLayout>
    )
  }
}

ClientsList = compose(
  graphql(clientActions.allClients, { name: "allClients" }),
  graphql(clientActions.createClient, {
    name: "createClient",
    options: {
      errorPolicy: "all"
    }
  })
)(ClientsList)

export default ClientsList
