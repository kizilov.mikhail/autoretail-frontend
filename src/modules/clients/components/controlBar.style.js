import styled from "styled-components"

const ControlBarWrapper = styled.div`
  margin-bottom: 20px;
`

export default ControlBarWrapper
