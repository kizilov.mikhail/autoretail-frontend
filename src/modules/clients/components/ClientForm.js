import React from "react"
import { Row, Col, Form, Modal, Input, Radio } from "antd"

import { Button } from "../../../components"

const FormItem = Form.Item
const RadioGroup = Radio.Group
const RadioButton = Radio.Button
const { TextArea } = Input

class ClientForm extends React.Component {
  componentDidMount() {
    const { client } = this.props
    const { setFieldsValue } = this.props.form
    setFieldsValue(client)
  }

  render() {
    const { disabled, handleEdit, handleSave, handleCancel } = this.props
    const { getFieldDecorator, resetFields } = this.props.form
    return (
      <Form className="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
        {disabled ? (
          <div class="m-form__actions m-form__actions--solid">
            <div className="col-lg-8">
              <span class="m-switch m-switch--icon m-switch--orange">
                <label>
                  <input type="checkbox" onClick={handleEdit} />
                  <span />
                </label>
              </span>
            </div>
          </div>
        ) : (
          <div class="m-form__actions m-form__actions--solid">
            <div className="col-lg-6">
              <Button
                color="success"
                className="m--margin-right-20"
                onClick={handleSave}
              >
                Сохранить
              </Button>
              <Button color="light" onClick={handleCancel}>
                Отменить
              </Button>
            </div>
          </div>
        )}
        <div class="form-group m-form__group row">
          <div class="col-lg-4">
            <label>Фамилия:</label>
            <FormItem>
              {getFieldDecorator("surname")(
                <input
                  type="text"
                  disabled={disabled}
                  class="form-control m-input"
                  placeholder="Enter full name"
                />
              )}
            </FormItem>
          </div>
          <div class="col-lg-4">
            <label>Имя:</label>
            <FormItem>
              {getFieldDecorator("name")(
                <input
                  type="text"
                  disabled={disabled}
                  class="form-control m-input"
                  placeholder="Enter full name"
                />
              )}
            </FormItem>
          </div>
          <div class="col-lg-4">
            <label>Отчество:</label>
            <FormItem>
              {getFieldDecorator("patronymic")(
                <input
                  type="text"
                  disabled={disabled}
                  class="form-control m-input"
                  placeholder="Enter full name"
                />
              )}
            </FormItem>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-6">
            <label>Телефон:</label>
            <FormItem>
              {getFieldDecorator("phone")(
                <div class="input-group m-input-group m-input-group--square">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="la la-phone" />
                    </span>
                  </div>
                  <input
                    type="text"
                    disabled={disabled}
                    class="form-control m-input"
                  />
                </div>
              )}
            </FormItem>
          </div>
          <div class="col-lg-6">
            <label>Электронная почта:</label>
            <FormItem>
              {getFieldDecorator("email")(
                <div class="input-group m-input-group m-input-group--square">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="la la-envelope-o" />
                    </span>
                  </div>
                  <input
                    type="text"
                    disabled={disabled}
                    class="form-control m-input"
                  />
                </div>
              )}
            </FormItem>
          </div>
        </div>
        <div class="form-group m-form__group row">
          <div class="col-lg-4">
            <label class="">Пол:</label>
            <FormItem>
              {getFieldDecorator("sex")(
                <div class="m-radio-inline">
                  <label class="m-radio m-radio--solid">
                    <input type="radio" value="male" /> Мужской
                    <span />
                  </label>
                  <label class="m-radio m-radio--solid">
                    <input type="radio" value="female" /> Женский
                    <span />
                  </label>
                </div>
              )}
            </FormItem>
          </div>
        </div>
      </Form>
    )
  }
}

export default Form.create()(ClientForm)
