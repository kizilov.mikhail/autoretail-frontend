import React from "react"
import { Button } from "antd"

export default ({ showModal, selected }) => {
  return (
    <div>
      <Button disabled={!selected}>Удалить</Button>
      <Button onClick={showModal}>Добавить</Button>
    </div>
  )
}
