import React from "react"
import { Row, Col, Form, Modal, Input, Radio } from "antd"
import { Button } from "../../../components"

const FormItem = Form.Item
const RadioGroup = Radio.Group
const RadioButton = Radio.Button

class AddForm extends React.Component {
  renderSexInput() {
    return (
      <RadioGroup initialValue="Мужской">
        <RadioButton value="Мужской">Мужчина</RadioButton>
        <RadioButton value="Женский">Женщина</RadioButton>
      </RadioGroup>
    )
  }

  render() {
    const options = {
      rules: [{ required: true, message: "Обязательное поле" }]
    }
    const { visible, onOk, onCancel } = this.props
    const { getFieldDecorator, resetFields } = this.props.form
    return (
      <Modal
        footer={[
          <Button color="success" onClick={onOk}>
            Создать
          </Button>,
          <Button color="danger" onClick={onCancel}>
            Отменить
          </Button>
        ]}
        width={700}
        title="Новый Клиент"
        okText="Создать"
        visible={visible}
        onOk={onOk}
        onCancel={onCancel}
        destroyOnClose={true}
      >
        <Form className="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
          <div class="form-group m-form__group row">
            <div class="col-lg-4">
              <label>Фамилия:</label>
              <FormItem hasFeedback>
                {getFieldDecorator("surname", options)(
                  <input
                    type="text"
                    class="form-control m-input"
                    placeholder="Фамилия"
                  />
                )}
              </FormItem>
            </div>
            <div class="col-lg-4">
              <label>Имя:</label>
              <FormItem hasFeedback>
                {getFieldDecorator("name", options)(
                  <input
                    type="text"
                    class="form-control m-input"
                    placeholder="Имя"
                  />
                )}
              </FormItem>
            </div>
            <div class="col-lg-4">
              <label>Отчество:</label>
              <FormItem>
                {getFieldDecorator("patronymic")(
                  <input
                    type="text"
                    class="form-control m-input"
                    placeholder="Отчество"
                  />
                )}
              </FormItem>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label>Телефон:</label>
              <FormItem hasFeedback>
                {getFieldDecorator("phone", options)(
                  <div class="input-group m-input-group m-input-group--square">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="la la-phone" />
                      </span>
                    </div>
                    <input type="text" class="form-control m-input" />
                  </div>
                )}
              </FormItem>
            </div>
            <div class="col-lg-6">
              <label>Электронная почта:</label>
              <FormItem>
                {getFieldDecorator("email")(
                  <div class="input-group m-input-group m-input-group--square">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="la la-envelope-o" />
                      </span>
                    </div>
                    <input type="text" class="form-control m-input" />
                  </div>
                )}
              </FormItem>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <div class="col-lg-6">
              <label class="">Пол:</label>
              <FormItem hasFeedback>
                {getFieldDecorator("sex", {
                  ...options,
                  initialValue: "Мужской"
                })(
                  <RadioGroup>
                    <Radio value="Мужской">Мужской</Radio>
                    <Radio value="Женский">Женский</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </div>
            <div className="col-lg-6">
              <FormItem>
                <label class="">Дата рождения:</label>
                {getFieldDecorator("birthDay")(
                  <input type="text" class="form-control m-input" />
                )}
              </FormItem>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <label>Комментарий:</label>
            <textarea class="form-control m-input" rows="3" />
          </div>
        </Form>
      </Modal>
    )
  }
}

export default Form.create()(AddForm)
