import React from "react"
import { Link } from "react-router-dom"
import Table from "../../../components/uielements/Table"
import { Button, Icon, Input } from "antd"

class ClientsTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      filterDropdownVisible: false,
      searchText: "",
      data: null,
      filtered: false
    }
  }
  onInputChange = e => {
    this.setState({ searchText: e.target.value })
  }
  onSearch = () => {
    const { data } = this.props
    const { searchText } = this.state
    const reg = new RegExp(searchText, "gi")
    this.setState({
      filterDropdownVisible: false,
      filtered: !!searchText,
      data: data
        .map(record => {
          const match = record.phone.match(reg)
          if (!match) {
            return null
          }
          return {
            ...record,
            name: (
              <span>
                {record.name
                  .split(reg)
                  .map(
                    (text, i) =>
                      i > 0
                        ? [<span className="highlight">{match[0]}</span>, text]
                        : text
                  )}
              </span>
            )
          }
        })
        .filter(record => !!record)
    })
  }
  render() {
    const columns = [
      {
        title: "ФИО",
        key: "fullName",
        render: (text, record) => (
          <span>
            {record.name} {record.surname} {record.patronymic}
          </span>
        )
      },
      {
        title: "День рождения",
        dataIndex: "birthDay"
      },
      {
        title: "Пол",
        dataIndex: "sex"
      },
      {
        title: "Телефон",
        dataIndex: "phone",
        filterDropdown: (
          <div className="custom-filter-dropdown">
            <Input
              ref={ele => (this.searchInput = ele)}
              placeholder="Search name"
              value={this.state.searchText}
              onChange={this.onInputChange}
              onPressEnter={this.onSearch}
            />
          </div>
        ),
        filterDropdownVisible: this.state.filterDropdownVisible,
        onFilterDropdownVisibleChange: visible => {
          this.setState(
            {
              filterDropdownVisible: visible
            },
            () => this.searchInput && this.searchInput.focus()
          )
        }
      },
      {
        title: "Email",
        dataIndex: "email"
      }
    ]
    const { rowSelection, loading, data, handleRowClick } = this.props
    return (
      <Table
        rowSelection={rowSelection}
        columns={columns}
        dataSource={this.state.data}
        rowKey="_id"
        pagination={false}
        loading={loading}
        onRow={(record, index) => ({
          onClick: () => handleRowClick(record._id),
          style: { cursor: "pointer" }
        })}
      />
    )
  }
}

export default ClientsTable
