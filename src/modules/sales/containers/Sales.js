import React from "react"
import { graphql, compose } from "react-apollo"
import { Modal, Menu, notification } from "antd"

import LayoutContentWrapper from "../../../app/components/LayoutContentWrapper.style"
import { PortletLayout, Button, MenuWrapper } from "../../../components"
import SalesTable from "../components/SalesTable"

class Sales extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      visible: false
    }
  }

  handleOk = e => {
    this.form.validateFields((err, values) => {
      if (!err) {
        this.props
          .createIncome({
            variables: { input: values }
          })
          .then(res => {
            const { errors, data: { createIncome } } = res
            if (errors) {
              notification.error({
                description: errors[0].message,
                message: "Ошибка создания Поступления"
              })
            } else {
              notification.success({ message: "Поступление создано" })
              this.setState({
                visible: false
              })
              this.form.resetFields()
              this.props.history.push(`incomes/${createIncome._id}`)
            }
          })
          .catch(err => {
            console.log("Failed")
          })
      }
    })
  }
  handleCancel = e => {
    this.setState({
      visible: false
    })
  }

  onMenuItemClick = ({ key }) => {
    //const { allIncomes: { refetch } } = this.props
    // const filter = key === "all" ? {} : { status: key }
    // refetch({ filter })
  }

  handleAdd = () => this.setState({ visible: true })

  handleDelete = () => {}

  render() {
    const { visible } = this.state
    const { match: { url } } = this.props

    return (
      <div style={{ display: "flex", height: "100%" }}>
        <MenuWrapper>
          <Menu
            mode="inline"
            onClick={this.onMenuItemClick}
            defaultSelectedKeys={["draft"]}
            inlineCollapsed={this.state.collapsed}
            style={{ width: "200px" }}
          >
            <Menu.Item key="draft">Новые</Menu.Item>
            <Menu.Item key="reserved">В работе</Menu.Item>
            <Menu.Item key="completed">Выданные</Menu.Item>
            <Menu.Item key="cancelled">Отмененные</Menu.Item>
            <Menu.Item key="all">Все сделки</Menu.Item>
          </Menu>
        </MenuWrapper>
        <LayoutContentWrapper style={{ flexGrow: "1" }}>
          <PortletLayout titleText="Сделки" history={this.props.history}>
            <div className="m--margin-bottom-20 m--align-right">
              <Button
                iconClass="flaticon-add"
                onClick={this.handleAdd}
                color="info"
              >
                Новая сделка
              </Button>
            </div>
            <SalesTable
              history={this.props.history}
              data={[]}
              loading={false}
            />
          </PortletLayout>
        </LayoutContentWrapper>
      </div>
    )
  }
}

export default compose()(Sales)
// graphql(incomeActions.allIncomes, {
//   name: "allIncomes",
//   options: { variables: { filter: { status: "draft" } } }
// }),
// graphql(incomeActions.createIncome, { name: "createIncome" }),
// graphql(inventoryActions.allInventories, { name: "allInventories" }),
// graphql(supplierActions.allSuppliers, { name: "allSuppliers" })
