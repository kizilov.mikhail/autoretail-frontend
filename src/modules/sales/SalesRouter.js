import React from "react"
import { Switch, Route } from "react-router-dom"

import Sales from "./containers/Sales"

class SalesRouter extends React.Component {
  render() {
    const { url } = this.props.match
    return (
      <Switch>
        <Route exact path={`${url}`} component={Sales} />
      </Switch>
    )
  }
}

export default SalesRouter
