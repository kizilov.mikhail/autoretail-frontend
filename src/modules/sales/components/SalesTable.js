import React from "react"
import { Link } from "react-router-dom"

import Table from "../../../components/uielements/Table"
import { normalizedDate } from "../../../utils"

const columns = [
  {
    title: "Номер/дата",
    dataIndex: "createdAt",
    render: date => normalizedDate(date)
  },
  { title: "Клиент", dataIndex: "client.name" },
  { title: "Со склада", dataIndex: "inventory.name" },
  { title: "Доставка", dataIndex: "delivery" },
  { title: "Сумма (руб.)", dataIndex: "total" },
  { title: "Статус", dataIndex: "status_" },
  { title: "Комментарий", dataIndex: "comment" },
  { title: "Источник", dataIndex: "#" },
  { title: "Ответственный", dataIndex: "post" }
]

const SalesTable = props => {
  const { history, data, loading } = props
  return (
    <Table
      onRow={(record, index) => ({
        onClick: () => {
          history.push(`sales/${record._id}`)
        },
        style: { cursor: "pointer" }
      })}
      scroll={{ x: "140%" }}
      columns={columns}
      dataSource={data}
      loading={loading}
      rowKey="_id"
      pagination={false}
    />
  )
}

export default SalesTable
