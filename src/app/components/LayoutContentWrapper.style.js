import styled from "styled-components"

const LayoutContentWrapper = styled.div`
  padding: 15px 15px;
  background-color: #f2f3f8;

  @media only screen and (max-width: 767px) {
    padding: 50px 20px;
  }

  @media (max-width: 580px) {
    padding: 15px;
  }
`

export default LayoutContentWrapper
