import React, { Component } from "react"
import { connect } from "react-redux"
import { Layout } from "antd"

import appActions from "../redux/app/actions"
import Sidebar from "./containers/Sidebar/Sidebar"
import Topbar from "./containers/Topbar/Topbar"
import AppStyle from "./App.style"
import AppRouter from "./AppRouter"
import "./global.css"
import "../styles/styles.bundle.css"
import "../styles/vendors.bundle.css"

const { Content } = Layout
const { toggleAll } = appActions

class App extends Component {
  render() {
    const { url } = this.props.match
    return (
      <AppStyle>
        <Layout style={{ height: "100vh" }}>
          <Topbar url={url} />
          <Layout style={{ flexDirection: "row", overflowX: "hidden" }}>
            <Sidebar url={url} />
            <Layout
              className="isoContentMainLayout"
              style={{
                height: "100vh"
              }}
            >
              <Content
                className="isomorphicContent"
                style={{
                  padding: "70px 0 0",
                  flexShrink: "0",
                  background: "#f1f3f6",
                  height: "100%"
                }}
              >
                <AppRouter url={url} />
              </Content>
            </Layout>
          </Layout>
        </Layout>
      </AppStyle>
    )
  }
}

export default connect(() => ({}), { toggleAll })(App)
