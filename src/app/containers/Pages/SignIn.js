import React, { Component } from "react"
import { Link, Redirect } from "react-router-dom"
import { Input, Button } from "antd"
import SignInWrapper from "./signin.style"

class SignIn extends Component {
  state = {
    redirectToReferrer: false
  }
  componentWillReceiveProps(nextProps) {
    if (
      this.props.isLoggedIn !== nextProps.isLoggedIn &&
      nextProps.isLoggedIn === true
    ) {
      this.setState({ redirectToReferrer: true })
    }
  }
  handleLogin = () => {
    this.props.history.push("/dashboard")
  }
  render() {
    const from = { pathname: "/dashboard" }
    const { redirectToReferrer } = this.state

    if (redirectToReferrer) {
      return <Redirect to={from} />
    }
    return (
      <SignInWrapper className="isoSignInPage">
        <div className="isoLoginContentWrapper">
          <div className="isoLoginContent">
            <div className="isoLogoWrapper">
              <Link to="/dashboard">Вход</Link>
            </div>

            <div className="isoSignInForm">
              <div className="isoInputWrapper">
                <Input size="large" placeholder="Телефон" />
              </div>

              <div className="isoInputWrapper">
                <Input size="large" type="password" placeholder="Пароль" />
              </div>

              <p className="isoHelperText">Текст подсказка</p>

              <div className="isoInputWrapper isoOtherLogin">
                <Button onClick={this.handleLogin} type="primary">
                  Войти
                </Button>
              </div>
              <div className="isoCenterComponent isoHelperWrapper">
                <Link to="" className="isoForgotPass">
                  Забыли пароль?
                </Link>
              </div>
            </div>
          </div>
        </div>
      </SignInWrapper>
    )
  }
}

export default SignIn
