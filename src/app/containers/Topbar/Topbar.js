import React, { Component } from "react"
import { connect } from "react-redux"
import { Layout } from "antd"

import appActions from "../../../redux/app/actions"
import TopbarWrapper from "./topbar.style"
import themeConfig from "../../../config/themeConfig"

const { topbarTheme } = themeConfig
const { Header } = Layout
const { toggleCollapsed } = appActions

class Topbar extends Component {
  render() {
    const { toggleCollapsed } = this.props
    const collapsed = this.props.collapsed && !this.props.openDrawer
    const styling = {
      background: topbarTheme.backgroundColor,
      position: "fixed",
      width: "100%",
      height: 70
    }
    return (
      <TopbarWrapper>
        <Header
          style={styling}
          className={
            collapsed ? "isomorphicTopbar collapsed" : "isomorphicTopbar"
          }
        >
          <div className="isoLeft">
            <button
              className={
                collapsed ? "triggerBtn menuCollapsed" : "triggerBtn menuOpen"
              }
              style={{ color: topbarTheme.textColor }}
              onClick={toggleCollapsed}
            />
          </div>

          <ul className="isoRight">
            <li
              onClick={() => this.setState({ selectedItem: "user" })}
              className="isoUser"
            >
              Место для юзера
            </li>
          </ul>
        </Header>
      </TopbarWrapper>
    )
  }
}

export default connect(
  state => ({
    ...state.App.toJS()
  }),
  { toggleCollapsed }
)(Topbar)
