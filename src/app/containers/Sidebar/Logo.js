import React from "react"
import { Link } from "react-router-dom"

export default ({ collapsed }) => (
  <div className="isoLogoWrapper">
    {collapsed ? (
      <div>
        <h3>
          <Link to="/dashboard">
            <i className="ion-flash" />
          </Link>
        </h3>
      </div>
    ) : (
      <h3>
        <Link to="/dashboard">AUTORETAIL</Link>
      </h3>
    )}
  </div>
)
