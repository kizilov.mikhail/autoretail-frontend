import React, { Component } from "react"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import { Scrollbars } from "react-custom-scrollbars"
import { Menu, Layout } from "antd"
import SidebarWrapper from "./sidebar.style"

import themeConfig from "../../../config/themeConfig"
import appActions from "../../../redux/app/actions"
import Logo from "./Logo"

const { sidebarTheme } = themeConfig
const { Sider } = Layout
const {
  toggleOpenDrawer,
  changeOpenKeys,
  changeCurrent,
  toggleCollapsed
} = appActions

class Sidebar extends Component {
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
    this.onOpenChange = this.onOpenChange.bind(this)
  }
  handleClick(e) {
    this.props.changeCurrent([e.key])
    if (this.props.app.view === "MobileView") {
      setTimeout(() => {
        this.props.toggleCollapsed()
        this.props.toggleOpenDrawer()
      }, 100)
    }
  }
  onOpenChange(newOpenKeys) {
    const { app, changeOpenKeys } = this.props
    const latestOpenKey = newOpenKeys.find(
      key => !(app.openKeys.indexOf(key) > -1)
    )
    const latestCloseKey = app.openKeys.find(
      key => !(newOpenKeys.indexOf(key) > -1)
    )
    let nextOpenKeys = []
    if (latestOpenKey) {
      nextOpenKeys = this.getAncestorKeys(latestOpenKey).concat(latestOpenKey)
    }
    if (latestCloseKey) {
      nextOpenKeys = this.getAncestorKeys(latestCloseKey)
    }
    changeOpenKeys(nextOpenKeys)
  }
  getAncestorKeys = key => {
    const map = {
      sub3: ["sub2"]
    }
    return map[key] || []
  }

  renderView({ style, ...props }) {
    const viewStyle = {
      marginRight: "-17px",
      paddingRight: "9px",
      marginLeft: "0",
      paddingLeft: "0"
    }
    return <div className="box" style={{ ...style, ...viewStyle }} {...props} />
  }

  render() {
    const { url, app, toggleOpenDrawer } = this.props
    //const collapsed = clone(app.collapsed) && !clone(app.openDrawer)
    const collapsed = app.collapsed && !app.openDrawer
    const { openDrawer } = app
    const mode = collapsed === true ? "vertical" : "inline"
    const onMouseEnter = event => {
      if (openDrawer === false) {
        toggleOpenDrawer()
      }
      return
    }
    const onMouseLeave = () => {
      if (openDrawer === true) {
        toggleOpenDrawer()
      }
      return
    }
    const scrollheight = app.height
    const styling = {
      backgroundColor: sidebarTheme.backgroundColor
    }
    const submenuColor = {
      color: sidebarTheme.textColor
    }
    return (
      <SidebarWrapper>
        <Sider
          trigger={null}
          collapsible={true}
          collapsed={collapsed}
          width="240"
          className="isomorphicSidebar"
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
          style={styling}
        >
          <Logo collapsed={collapsed} />
          <Scrollbars
            renderView={this.renderView}
            style={{ height: scrollheight - 70 }}
          >
            <Menu
              onClick={this.handleClick}
              theme="dark"
              mode={mode}
              openKeys={collapsed ? [] : app.openKeys}
              selectedKeys={app.current}
              onOpenChange={this.onOpenChange}
              className="isoDashboardMenu"
            >
              <Menu.Item key="clients">
                <Link to={`${url}/clients`}>
                  <span className="isoMenuHolder" style={submenuColor}>
                    <i className="ion-person" />
                    <span className="nav-text">Клиенты</span>
                  </span>
                </Link>
              </Menu.Item>
              <Menu.Item key="cars">
                <Link to={`${url}/cars`}>
                  <span className="isoMenuHolder" style={submenuColor}>
                    <i className="ion-model-s" />
                    <span className="nav-text">Автомобили</span>
                  </span>
                </Link>
              </Menu.Item>
              <Menu.Item key="products">
                <Link to={`${url}/products`}>
                  <span className="isoMenuHolder" style={submenuColor}>
                    <i className="ion-bag" />
                    <span className="nav-text">Товары</span>
                  </span>
                </Link>
              </Menu.Item>
              <Menu.Item key="sales">
                <Link to={`${url}/sales`}>
                  <span className="isoMenuHolder" style={submenuColor}>
                    <i className="ion-card" />
                    <span className="nav-text">Сделки</span>
                  </span>
                </Link>
              </Menu.Item>
              <Menu.Item key="incomes">
                <Link to={`${url}/incomes`}>
                  <span className="isoMenuHolder" style={submenuColor}>
                    <i className="ion-cube" />
                    <span className="nav-text">Поступления</span>
                  </span>
                </Link>
              </Menu.Item>
              <Menu.Item key="settings">
                <Link to={`${url}/settings`}>
                  <span className="isoMenuHolder" style={submenuColor}>
                    <i className="ion-settings" />
                    <span className="nav-text">Настройки</span>
                  </span>
                </Link>
              </Menu.Item>
            </Menu>
          </Scrollbars>
        </Sider>
      </SidebarWrapper>
    )
  }
}

export default connect(
  state => ({
    app: state.App.toJS()
  }),
  { toggleOpenDrawer, changeOpenKeys, changeCurrent, toggleCollapsed }
)(Sidebar)
