import React from "react"
import { Switch, Route } from "react-router-dom"

import { ClientsRouter } from "../modules/clients"
import { CarsRouter } from "../modules/cars"
import { IncomesRouter } from "../modules/incomes"
import { SettingsRouter } from "../modules/settings"
import { ProductsRouter } from "../modules/products"
import { SalesRouter } from "../modules/sales"

class AppRouter extends React.Component {
  render() {
    const { url } = this.props
    return (
      <Switch>
        <Route path={`${url}/clients`} component={ClientsRouter} />
        <Route path={`${url}/cars`} component={CarsRouter} />
        <Route path={`${url}/incomes`} component={IncomesRouter} />
        <Route path={`${url}/settings`} component={SettingsRouter} />
        <Route path={`${url}/products`} component={ProductsRouter} />
        <Route path={`${url}/sales`} component={SalesRouter} />
      </Switch>
    )
  }
}

export default AppRouter
