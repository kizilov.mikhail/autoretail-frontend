import categoryActions from "./categoryActions"
import supplierActions from "./supplierActions"
import inventoryActions from "./inventoryActions"
import clientActions from "./clientActions"
import carActions from "./carActions"
import incomeActions from "./incomeActions"
import productActions from "./productActions"
import carCatalogActions from "./carCatalogActions"

export {
  clientActions,
  carActions,
  categoryActions,
  supplierActions,
  inventoryActions,
  incomeActions,
  productActions,
  carCatalogActions
}
