import gql from "graphql-tag"

const allClients = gql`
  {
    allClients {
      _id
      name
      surname
      patronymic
      sex
      birthDay
      phone
      email
    }
  }
`

const createClient = gql`
  mutation createClient($input: ClientInput!) {
    createClient(input: $input) {
      _id
      name
      surname
      patronymic
      sex
      birthDay
      phone
      email
    }
  }
`

export default { allClients, createClient }
