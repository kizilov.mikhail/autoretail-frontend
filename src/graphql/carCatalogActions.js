import gql from "graphql-tag"

const carSuggestions = gql`
  query carSuggestions(
    $brand: String
    $model: String
    $body: String
    $engine: String
  ) {
    carSuggestions(brand: $brand, model: $model, body: $body, engine: $engine) {
      _id
      category
      name
    }
  }
`

export default { carSuggestions }
