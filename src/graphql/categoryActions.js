import gql from "graphql-tag"

const allCategories = gql`
  {
    allCategories {
      _id
      name
    }
  }
`

const createCategory = gql`
  mutation createCategory($input: CreateCategory) {
    createCategory(input: $input) {
      _id
      name
    }
  }
`

const deleteCategories = gql`
  mutation deleteCategories($ids: [String]!) {
    deleteCategories(ids: $ids) {
      _id
    }
  }
`

export default { allCategories, createCategory, deleteCategories }
