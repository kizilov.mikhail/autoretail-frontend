import gql from "graphql-tag"

const allIncomes = gql`
  query allIncomes($filter: FilterIncomes) {
    allIncomes(filter: $filter) {
      _id
      supplier {
        _id
        name
      }
      inventory {
        _id
        name
      }
      status
      status_
      total
      comment
      createdAt
    }
  }
`

const createIncome = gql`
  mutation createIncome($input: CreateIncome!) {
    createIncome(input: $input) {
      _id
      supplier {
        _id
        name
      }
      inventory {
        _id
        name
      }
      status
      status_
      total
      comment
    }
  }
`

const updateIncomeStatus = gql`
  mutation updateIncomeStatus($input: UpdateIncomeStatus!) {
    updateIncomeStatus(input: $input) {
      _id
      status
      status_
    }
  }
`

const income = gql`
  query income($id: String!) {
    income(id: $id) {
      _id
      inventory {
        _id
        name
      }
      supplier {
        _id
        name
      }
      products {
        _id
        category
        brand
        model
        body
        direction
        direction_
        side
        side_
        level
        level_
        quantity
        price
      }
      status
      status_
      total
      comment
      createdAt
    }
  }
`

export default { allIncomes, createIncome, updateIncomeStatus, income }
