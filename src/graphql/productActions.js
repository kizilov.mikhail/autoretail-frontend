import gql from "graphql-tag"

const allProducts = gql`
  query allProducts {
    allProducts {
      _id
      income {
        supplier {
          name
        }
      }
      inventory {
        name
      }
      category
      brand
      model
      body
      engine
      year
      direction
      direction_
      side
      side_
      level
      level_
      quantity
      price
      status
      comment
    }
  }
`
const createProduct = gql`
  mutation createProduct($input: CreateProduct!) {
    createProduct(input: $input) {
      _id
      inventory {
        name
      }
      category
      brand
      model
      body
      engine
      year
      direction
      direction_
      side
      side_
      level
      level_
      price
      status
      comment
    }
  }
`

export default { createProduct, allProducts }
