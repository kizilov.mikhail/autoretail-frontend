import gql from "graphql-tag"

const allSuppliers = gql`
  {
    allSuppliers {
      _id
      name
    }
  }
`

const createSupplier = gql`
  mutation createSupplier($input: CreateSupplier) {
    createSupplier(input: $input) {
      _id
      name
    }
  }
`

const deleteCategories = gql`
  mutation deleteCategories($ids: [String]!) {
    deleteCategories(ids: $ids) {
      _id
    }
  }
`

export default { allSuppliers, createSupplier, deleteCategories }
