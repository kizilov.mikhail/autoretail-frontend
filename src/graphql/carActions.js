import gql from "graphql-tag"

const allCars = gql`
  {
    allCars {
      _id
      brand
      model
      body
      engine
      stateNumber
      region
      vin
      bodyNumber
      owner {
        _id
        name
        surname
        patronymic
        phone
      }
    }
  }
`

const createCar = gql`
  mutation createCar($input: CarInput!) {
    createCar(input: $input) {
      _id
      brand
      model
      body
      engine
      stateNumber
      region
      vin
      bodyNumber
    }
  }
`

export default { allCars, createCar }
