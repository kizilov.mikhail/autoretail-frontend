import gql from "graphql-tag"

const allInventories = gql`
  {
    allInventories {
      _id
      name
    }
  }
`

const createInventory = gql`
  mutation createInventory($input: CreateInventory) {
    createInventory(input: $input) {
      _id
      name
    }
  }
`

const deleteInventory = gql`
  mutation deleteCategories($ids: [String]!) {
    deleteCategories(ids: $ids) {
      _id
    }
  }
`

export default { allInventories, createInventory, deleteInventory }
